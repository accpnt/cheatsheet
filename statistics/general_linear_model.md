# General Linear Model (GLM)

## Overview

The general linear model or multivariate regression model is a statistical linear model. It may be written as 

Y = X B + U

* Y is a matrix with series of multivariate measurements (each column being a set of measurements on one of the dependent variables)
    * Dependant Variable
    * Describes a response
* X is a matrix of observations on independent variables that might be a design matrix (each column being a set of observations on one of the independent variables)
    * Independant Variables
    * aka. Predictor
    * e.g : Experimental conditions (embodies all available knowledge about experimentally controlled factors and potential confounds)
* B is a matrix containing parameters that are usually to be estimated
    * Parameters
    * aka regression coefficient/beta weights
    * Quantifies how much each predictor (X) independently influences the dependent variable (Y)
    * The slope of the line
    * The parameter B chosen for a model should minimise the error (reducing the amount of variance in y which is left unexplained) 
* U is a matrix containing errors (noise). 
    * Variance in the data (y) which is not explained by the linear combination of predictors (x)
    * Uncorrelated across measurements
    * Follow a multivariate normal distribution. 
    * If the errors do not follow a multivariate normal distribution, generalized linear models may be used to relax assumptions about Y and U. 

## GLM Assumptions

1. Errors are normally distributed - smoothing
2. Error is the same in each & every measurement point
3. There is no correlation between errors at different time points/data points

## Summary

|                | Compare                                 | By                                               | Example                                                                                     |
|----------------|-----------------------------------------|--------------------------------------------------|---------------------------------------------------------------------------------------|
| One-way ANOVA  | 1 continuous response variable          | 1 categorical variable                           | comparing test score by religion                                                      |
| Two-way ANOVA  | 1 continuous response variable          | 2 or more categorical variables                  | comparing test score by both religion and race                                        |
| ANCOVA         | 1 continuous response variable          | 1 categorical variable and 1 continuous variable | comparing test score by both religion and ‘number of hours working’                   |
| One-way MANOVA | 2 or more continuous response variables | 1 categorical variable                           | comparing test score and annual income by religion                                    |
| Two-way MANOVA | 2 or more continuous response variables | 2 or more categorical variables                  | comparing test score and annual income by both religion and race                      |
| MANCOVA        | 2 or more continuous response variables | 1 categorical variable and 1 continuous variable | comparing test score and annual income by both religion and ‘number of hours working’ |

## References

[Statistical Soup: ANOVA, ANCOVA, MANOVA, & MANCOVA](http://www.statsmakemecry.com/smmctheblog/stats-soup-anova-ancova-manova-mancova)

[A summary of ANOVA, ANCOVA, MANOVA & MANCOVA](http://www.jzliu.net/blog/summary-anova-ancova-manova-mancova/)

[The General Linear Model](https://www.fil.ion.ucl.ac.uk/mfd_archive/2011/page1/mfd2011_GLM.pdf)
