# Survival Analysis

## Introduction

Survival analysis is a branch of statistics for analyzing the expected duration of time until one or more events happen, such as death in biological organisms and failure in mechanical systems. 

The following terms are commonly used in survival analyses:

* Event: Death, disease occurrence, disease recurrence, recovery, or other experience of interest
* Time: The time from the beginning of an observation period (such as surgery or beginning treatment) to (i) an event, or (ii) end of the study, or (iii) loss of contact or withdrawal from the study.
* Censoring / Censored observation: If a subject does not have an event during the observation time, they are described as censored. The subject is censored in the sense that nothing is observed or known about that subject after the time of censoring. A censored subject may or may not have an event after the end of observation time.
* Survival function S(t): The probability that a subject survives longer than time t.

Survival analysis is used in several ways:

* To describe the survival times of members of a group
    * Life tables
    * Kaplan-Meier curves
    * Survival function
    * Hazard function
* To compare the survival times of two or more groups
    * Log-rank test
* To describe the effect of categorical or quantitative variables on survival
    * Cox proportional hazards regression
    * Parametric survival models
    * Survival trees
    * Survival random forests

## General formulation

### Survival function

The object of primary interest is the survival function, conventionally denoted S, which is defined as :

$`S(t) = P(T > t)`$

where t is some time, T is a random variable denoting the time of death. 

### Lifetime distribution function and event density

The lifetime distribution function, conventionally denoted F, is defined as the complement of the survival function : 

$`F(t) = 1 - S(t)`$

The density function of the lifetime distribution, is conventionally denoted f : 

$`f(t) = F'(t) = \frac{dF(t)}{dt}`$

We then have : 

$`S(t) = P(T > t) = \int_{t}^{\infty} f(u)du = 1 - F(t)`$

### Hazard function and cumulative hazard function

The hazard function, conventionally denoted $`\lambda`$, is defined as the event rate at time t conditional on survival until time t or later (that is, T > t). Suppose that an item has survived for a time t and we desire the probability that it will not survive for an additional time dt: 

$`\lambda(t) = \lim_{dt\to0} \frac{P(t \le T \le t + dt)}{dt S(t)} = \frac{f(t)}{S(t)} = - \frac{S'(t)}{S(t)}`$

## Parametric models

### Principle of parametric survival model

The principle of the parametric survival regression is to link the survival time of an individual to covariates using a specified probability distribution (generally the Weibull distribution). For example, in the medical domain, we are seeking to find out which covariate has the most important impact on the survival time of a patient.

### Parametric survival models

A parametric survival model is a well-recognized statistical technique for exploring the relationship between the survival of a patient, a parametric distribution and several explanatory variables. It allows us to estimate the parameters of the distribution.

| Distribution | f(t)                                          | h(t)                           | H(t)                          | S(t)                               |
|--------------|-----------------------------------------------|--------------------------------|-------------------------------|------------------------------------|
| Exponential  | $`\lambda exp(-\lambda t)`$                   | $`\lambda`$                    | $`\lambda t`$                 | $`exp(-\lambda t)`$                |
| Weibull      | $`\lambda pt^{p-1} exp(-\lambda t^p)`$        | $`\lambda pt^{p-1}`$           | $`\lambda t^p`$               | $`exp(-(\lambda t)^p)`$            |
| Gompertz     | $`a exp(bt) exp(-\frac{a}{b} (exp(bt) - 1))`$ | $`a exp(bt)`$                  | $`\frac{a}{b} (exp(bt) - 1)`$ | $`exp(-\frac{a}{b} (exp(bt - 1))`$ |
| Log-logistic | $`\frac{abt^{b-1}}{(1 + at^b)^2}`$            | $`\frac{abt^{b-1}}{1 + at^b}`$ | $`log(1 + at^b)`$             | $`(1 + at^b)^{-1}`$                |

* f(t) instantaneous failure rate.
* h(t) instantaneous hazard.
* H(t) cumulative hazard.
* S(t) survival.

#### Exponential model

The exponential model is the simplest type of parametric model in that it assumes that the baseline hazard is constant over time:

$`h(t) = h_0 exp(\beta X)`$,  where $`h_0 = \lambda`$

The assumption that the baseline hazard is constant over time can be evaluated in several ways. The first method is to generate an estimate of the baseline hazard from a Cox proportional hazards model and plot it to check if it follows a straight, horizontal line. A second approach is to fit a model with a piecewise-constant baseline hazard. Here, the baseline hazard is allowed to vary across time intervals (by including indicator variables for each of the time intervals). The baseline hazard is assumed to be constant within each time period, but can vary between time periods.

#### Weibull model

In a Weibull model it is assumed that the baseline hazard has a shape which gives rise to a Weibull distribution of survival times:

$`h(t) = h_0 exp(\beta X)`$,  where $`h_0 = \lambda p t^{p-1}`$

Where $`\beta X`$,  where $`h_0 = \lambda p t^{p-1}`$ includes an intercept term $`\beta_0`$. The suitability of the assumption that survival times follow a Weibull distribution can be assessed by generating a log-cumulative hazard plot. If the distribution is Weibull, this function will follow a straight line. The estimated shape parameter from the Weibull model gives an indication of whether hazard is falling (p < 1), constant (p = 1), or increasing (p > 1) over time.

#### Accelerated failure time models

The general form of an accelerated failure time model is:

$`log(t) = \beta X + log(\tau)`$ or $`t = exp(\beta X) \tau`$

where log(t) is the natural log of the time to failure event, $`\beta X`$ is a linear combination of explanatory variables and log ($`\tau`$) is an error term. Using this approach $`\tau`$ is the distribution of survival times when $`\beta X = 0`$. If we assume that $`\tau`$ follows a log-normal distribution, then the log of survival times will have a normal distribution, which is equivalent to fitting a linear model to the natural log of survival time (assuming that you can ignore the problem of dealing with censored observations). The former equation can be expressed as follow : 

$`\tau = exp(-\beta X) t`$ or $`ln(\tau) = \beta X + log(\tau)`$

The linear combination of predictors in the model ($`\beta X`$) can act additively or multiplicatively on the log of time: they speed up or slow down time to event by a multiplicative factor. In this case $`exp(-\beta X)`$ is called the acceleration parameter such that if $`exp(-\beta X)`$ > 1 time passes more quickly, if $`exp(-\beta X)`$ = 1 time passes at a normal rate, and if $`exp(-\beta X)`$ < 1 time passes more slowly.

### Variables selection for the parametric survival regression

It is possible to improve the parametric survival model by selecting the variables being part of the model. 

* Forward selection: The selection process starts by adding the variable with the largest contribution to the model. If a second variable is such that its entry probability is greater than the entry threshold value, then it is added to the model. This process is iterated until no new variable can be entered in the model.
* Backward selection: This method is similar to the previous one but starts from a complete model.

## Life table analysis

### What is life table analysis

Life table analysis belongs to the descriptive methods of survival analysis, similarly to the Kaplan Meier analysis. The life table analysis method was developed first, but the Kaplan-Meier analysis method has been shown to be superior in many cases.

Life table analysis allows to quickly obtain a population survival curve and essential statistics such as the median survival time. Life table analysis, which main result is the life table (also called actuarial table) works on regular time intervals. This is the major difference with the Kaplan Meier analysis, where the time intervals are taken as they are in the data set.

### Use of Life table analysis

Life table analysis allows you to analyze how a given population evolves with time. This technique is mostly applied to survival data and product quality data. There are three main reasons why a population of individuals or products may evolve:

* some individuals die (products fail),
* some other go out of the surveyed population because they get healed (repaired) or because their trace is lost (individuals move from location, the study is terminated, among other possible reasons).

The first type of data is usually called failure data, or event data, while the second is called censored data.

The life table method allows comparing populations, through their survival curves. For example, it can be of interest to compare the survival times of two samples of the same product produced in two different locations. Tests can be performed to check if the survival curves have arisen from identical survival functions. These results can later be used to model the survival curves and to predict probabilities of failure.

### Censoring data for life table analysis

#### Types of censoring for life table analysis

There are several types of censoring of survival data:

* Left censoring: when an event is reported at time t=t(i), we know that the event occurred at t * t(i).
* Right censoring: when an event is reported at time t=t(i), we know that the event occurred at t * t(i), if it ever occurred.
* Interval censoring: when an event is reported at time t=t(i), we know that the event occurred during [t(i-1); t(i)].
* Exact censoring: when an event is reported at time t=t(i), we know that the event occurred exactly at t=t(i).

#### Independent censoring for life table analysis

The life table method requires that the observations are independent. Second, the censoring must be independent: if you consider two random individuals in the study at time t-1, if one of the individuals is censored at time t, and if the other survives, then both must have equal chances to survive at time t. There are four different types of independent censoring:

* Simple type I: all individuals are censored at the same time or equivalently individuals are followed during a fixed time interval.
* Progressive type I: all individuals are censored at the same date (for example, when the study terminates).
* Type II: the study is continued until n events have been recorded.
* Random: the time when a censoring occurs is independent of the survival time.

## Kaplan-Meier analysis

### What is Kaplan-Meier analysis

The Kaplan–Meier estimator, also known as the product limit estimator, is a non-parametric statistic used to estimate the survival function from lifetime data.

The Kaplan-Meier method, also called product-limit analysis, belongs to the descriptive methods of survival analysis, as does life table analysis. The life table analysis method was developed first, but the Kaplan-Meier method has been shown to be superior in many cases.

Kaplan-Meier analysis allows you to quickly obtain a population survival curve and essential statistics such as the median survival time. Kaplan-Meier analysis, which main result is the Kaplan-Meier table, is based on irregular time intervals, contrary to the life table analysis, where the time intervals are regular.

### Formulation

The Kaplan-Meier (KM) method is a non-parametric method used to estimate the survival probability from observed survival times.

The survival probability at time ti, $`S(t_i)`$, is calculated as follow:

$`S(t_i) = S(t_{i-1})(1 - \frac{d_i}{n_i})`$

Where,

* $`S(t_i)`$ = the probability of being alive at $`t_{i-1}`$
* $`n_i`$ = the number of patients alive just before $`t_i`$
* $`d_i`$ = the number of events at  $`t_i`$
* $`t_0 = 0`$, $`S(0) = 1`$

The estimated probability (S(t)) is a step function that changes value only at the time of each event. It’s also possible to compute confidence intervals for the survival probability.

The KM survival curve, a plot of the KM survival probability against time, provides a useful summary of the data that can be used to estimate measures such as median survival time.

The unconditional probability of surviving past any time t is estimated by : 

$`\hat{S}(t) = \prod\limits_{t_i \le t} \frac{n_i - d_i}{n_i}`$

### Use 

Kaplan-Meier analysis is used to analyze how a given population evolves with time. This technique is mostly applied to survival data and product quality data. There are three main reasons why a population of individuals or products may evolve: some individuals die (products fail), some other go out of the surveyed population because they get healed (repaired) or because their trace is lost (individuals move from location, the study is terminated, among other reasons). The first type of data is usually called failure data, or event data, while the second is called censored data.

The Kaplan-Meier analysis allows you to compare populations, through their survival curves. For example, it can be of interest to compare the survival times of two samples of the same product produced in two different locations. Tests can be performed to check if the survival curves have arisen from identical survival functions. These results can later be used to model the survival curves and to predict probabilities of failure.

### Censoring data

#### Types of censoring

There are several types of censoring of survival data:

* Left censoring: when an event is reported at time t=t(i), we know that the event occurred at t * t(i).
* Right censoring: when an event is reported at time t=t(i), we know that the event occurred at t * t(i), if it ever occurred.
* Interval censoring: when an event is reported at time t=t(i), we know that the event occurred during [t(i-1); t(i)].
* Exact censoring: when an event is reported at time t=t(i), we know that the event occurred exactly at t=t(i).

#### Independent censoring

The Kaplan-Meier method requires that the observations are independent. Second, the censoring must be independent: if you consider two random individuals in the study at time t-1, if one of the individuals is censored at time t, and if the other survives, then both must have equal chances to survive at time t. There are four different types of independent censoring:

* Simple type I: all individuals are censored at the same time or equivalently individuals are followed during a fixed time interval.
* Progressive type I: all individuals are censored at the same date (for example, when the study terminates).
* Type II: the study is continued until n events have been recorded.
* Random: the time when a censoring occurs is independent of the survival time.

## Proportional Hazards Model with interval censored data

Use the interval-censored proportional hazard model to model survival time based on quantitative or qualitative explanatory variables.

### What is Proportional Hazards Model with interval censored data? 

The principle of the proportional hazards model is to link the survival time of an individual to covariates. For example, in the medical domain, we are seeking to find out which covariate has the most important impact on the survival time of a patient.

The first proportional hazard model, introduced by Cox in 1972, works with uncensored data and right censored data. The purpose of the proportional hazard model with interval censored data is, therefore, the same as for the Cox model, but it will also be possible to model survival times for interval-censored data, uncensored data, left censored data or right censored data.

If the data contains only uncensored or right-censored observations, it is possible, with this function, to reproduce the results of a Cox model. However, it is recommended to use Cox's proportional hazards model as it provides a more suitable method for this type of case. Both quantitative and qualitativeexplanatory variables can be taken into account.

### Cox Proportional-Hazards Model

#### Basics

The purpose of the model is to evaluate simultaneously the effect of several factors on survival. In other words, it allows us to examine how specified factors influence the rate of a particular event happening (e.g., infection, death) at a particular point in time. This rate is commonly referred as the hazard rate. Predictor variables (or factors) are usually termed covariates in the survival-analysis literature.

The Cox model is expressed by the hazard function denoted by h(t). Briefly, the hazard function can be interpreted as the risk of dying at time t. It can be estimated as follow:

$`\lambda(t) = \lambda_0(t) exp(b_1 x_1 + b_2 x_2 + ... + b_p x_p)`$

where,

* t represents the survival time
* $`\lambda(t)`$ is the hazard function determined by a set of p covariates (x1,x2,...,xp)
* the coefficients (b1,b2,...,bp) measure the impact (i.e., the effect size) of covariates.
* the term $`\lambda_0(t)`$ is called the baseline hazard. It corresponds to the value of the hazard if all the xi are equal to zero (the quantity exp(0) equals 1). The ‘t’ in $`\lambda(t)`$ reminds us that the hazard may vary over time.

The Cox model can be written as a multiple linear regression of the logarithm of the hazard on the variables xi, with the baseline hazard being an ‘intercept’ term that varies with time.

The quantities exp(bi) are called hazard ratios (HR). A value of bi greater than zero, or equivalently a hazard ratio greater than one, indicates that as the value of the ith covariate increases, the event hazard increases and thus the length of survival decreases.

Put another way, a hazard ratio above 1 indicates a covariate that is positively associated with the event probability, and thus negatively associated with the length of survival.

In summary,

* HR = 1: No effect
* HR < 1: Reduction in the hazard
* HR > 1: Increase in Hazard

#### Testing the proportional hazards assumption

Once a suitable set of covariates has been identified, it is wise to check each covariate to ensure that the proportional hazards assumption is valid. To assess the proportional hazards assumption we examine the extent to which the estimated hazard curves for each level of strata of a covariate are equidistant over time.

A plot of the scaled Schoenfeld residuals (and a loess smoother) as a function of time may be used to test proportionality of hazards. In a ‘well-behaved’ model the Schoenfeld residuals are scattered around 0 and a regression line fitted to the residuals has a slope of approximately 0. The idea behind this test is that if the proportional hazards assumption holds for a particular covariate then the Schoenfeld residuals for that covariate will not be related to survival time. 

The implementation of the test can be thought of as a three-step process: (1) run a Cox proportional hazards model and obtain the Schoenfeld residuals for each predictor, (2) create a variable that ranks the order of failures (the subject who has the first (earliest) event gets a value of 1, the next gets a value of 2, and so on, (3) test the correlation between the variables created in the first and second steps. The null hypothesis is that the correlation between the Schoenfeld residuals and ranked failure time is zero. An important point about this approach is that the null hypothesis is never proven with a statistical test (the most that can be said is that there is not enough evidence to reject the null) and that p-values are driven by sample size. A gross violation of the null assumption may not be statistically significant if the sample is very small. Conversely, a slight violation of the null assumption may be highly significant if the sample is very large.

For categorical covariates the proportional hazards assumption can be visually tested by plotting -log[-log S(t)] vs time for strata of each covariate. If the proportionality assumption holds the two (or more) curves should be approximately parallel and should not cross. Alternatively, run a model with each covariate (individually). Introduce a time-dependent interaction term for that covariate. If the proportional hazards assumption is valid for the covariate, the introduction of the time-dependent interaction term won’t be significant. This approach is regarded as the most sensitive (and objective) method for testing the proportional hazards assumption.

What do you do if a covariate violates the proportional hazards assumption? The first option is to stratify the model by the offending covariate. This means that a separate baseline hazard function is produced for each level of the covariate. Note you can’t obtain a hazard ratio for the covariate you’ve stratified on because its influence on survival is ‘absorbed’ into the (two or more) baseline hazard functions in the stratified model. If you are interested in quantifying the effect of the covariate on survival then you should introduce a time-dependent interaction term for the covariate, as described above.

#### Residuals

Residuals analysis provide information for evaluating a fitted proportional hazards model. They identify leverage and influence measures and can be used to assess the proportional hazards assumption. By definition, residuals for censored observations are negative and residual plots are useful to get a feeling for the amount of censoring in the data set — large amounts of censoring will result in ‘banding’ of the residual points. There are three types of residuals:

1. Martingale residuals. Martingale residuals are the difference between the observed number of events for an individual and the conditionally expected number given the fitted model, follow up time, and the observed course of any time-varying covariates. Martingale residuals may be plotted against covariates to detect non-linearity (that is, an incorrectly specified functional form in the parametric part of the model). Martingale residuals are sometimes referred to as Cox-Snell or modified Cox-Snell residuals.
2. Score residuals. Score residuals should be thought of as a three-way array with dimensions of subject, covariate and time. Score residuals are useful for assessing individual influence and for robust variance estimation.
3. Schoenfeld residuals. Schoenfeld residuals are useful for assessing proportional hazards. Schoenfeld residuals provide greater diagnostic power than unscaled residuals. Sometimes referred to as score residuals.

## Logrank test

The logrank test is a hypothesis test to compare the survival distributions of two samples. It is a nonparametric test and appropriate to use when the data are right skewed and censored (technically, the censoring must be non-informative).

The logrank test statistic compares estimates of the hazard functions of the two groups at each observed event time. It is constructed by computing the observed and expected number of events in one of the groups at each observed event time and then adding these to obtain an overall summary across all-time points where there is an event. It tests the null hypothesis that the survival curves in the two groups are the same. 

## References 

[Survival plots with survminer in R](http://www.sthda.com/english/rpkgs/survminer/survminer_cheatsheet.pdf)

[Survival Analysis Basics ](http://www.sthda.com/english/wiki/survival-analysis-basics)

[Survival Analysis for Business Analytics](https://www.kdnuggets.com/2017/11/survival-analysis-business-analytics.html)

[Survival Analysis](http://www.sthda.com/english/wiki/survival-analysis)

[Kaplan-Meier using SPSS Statistics](https://statistics.laerd.com/spss-tutorials/kaplan-meier-using-spss-statistics.php)

[Cox Proportional-Hazards Model](http://www.sthda.com/english/wiki/cox-proportional-hazards-model)

