# p-value

## What Are p-values?

### In simple words...

p-value = probability of observing a test statistic at least as large as the one calculated assuming the null hypothesis is true

p-value = probability to observe the value we observe or worst under the H0 hypothesis

p-value = probability to observe what we observe or worst by chance

p-value = probability to fail by wrongly rejecting H0

If the p-value < $`\alpha`$, we reject H0. 

In other words: being in the rejection area si equivalent of having p-value < $`\alpha`$

In other words: being in the rejection area si equivalent of being out of the confident interval of level $`1 - \alpha`$

### Graphical approach

P-values are the probability of obtaining an effect at least as extreme as the one in your sample data, assuming the truth of the null hypothesis. 

On the probability distribution plot, the significance level defines how far the sample value must be from the null value before we can reject the null. The percentage of the area under the curve that is shaded equals the probability that the sample value will fall in those regions if the null hypothesis is correct. 

![critical_region](./assets/critical_region.png)

The two shaded regions in the graph are equidistant from the central value of the null hypothesis. Each region has a probability of 0.025, which sums to our desired total of 0.05. These shaded areas are called the critical region for a two-tailed hypothesis test.

The critical region defines sample values that are improbable enough to warrant rejecting the null hypothesis. If the null hypothesis is correct and the population mean is 260, random samples (n=25) from this population have means that fall in the critical region 5% of the time.

The sample mean is statistically significant at the 0.05 level because it falls in the critical region.

![p_value](./assets/p-value.png)

### Interpret the p-value

In statistical hypothesis testing, the p-value or probability value is, for a given statistical model, the probability that, when the null hypothesis is true, the statistical summary (such as the sample mean difference between two groups) would be equal to, or more extreme than, the actual observed results.

The p-value is compared to the pre-chosen alpha value. A result is statistically significant when the p-value is less than alpha. This signifies a change was detected: that the default hypothesis can be rejected.

* If p-value > alpha: Fail to reject the null hypothesis (i.e. not signifiant result).
* If p-value <= alpha: Reject the null hypothesis (i.e. significant result).
