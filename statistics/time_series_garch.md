# Time Series (ARCH/GARCH)

## ARCH (AutoRegressive Conditional Heteroskedasticity)

### Definition

In econometrics, the autoregressive conditional heteroscedasticity (ARCH) model is a statistical model for time series data that describes the variance of the current error term or innovation as a function of the actual sizes of the previous time periods' error terms; often the variance is related to the squares of the previous innovations. The ARCH model is appropriate when the error variance in a time series follows an autoregressive (AR) model. 

To model a time series using an ARCH process, let $`\epsilon_{t}`$ denote the error terms (return residuals, with respect to a mean process), i.e. the series terms. These $`\epsilon _{t}`$ are split into a stochastic piece $`z_{t}`$ and a time-dependent standard deviation $`\sigma_{t}`$ characterizing the typical size of the terms so that

$`\epsilon_{t} = \sigma_{t} z_{t}`$

The random variable $`z_{t}`$ is a strong white noise process. The series $`\sigma_{t}^{2}`$ is modeled by

$`\sigma_{t}^{2} = \alpha_{0} + \alpha_{1} \epsilon_{{t-1}}^{2} + \cdots + \alpha_{q} \epsilon_{{t-q}}^{2} = \alpha_{0} + \sum_{{i=1}}^{q} \alpha_{{i}}\epsilon_{{t-i}}^{2}`$, where $`\alpha_{0} > 0`$ and $`\alpha_{i} \geq 0`$, i > 0.

### Estimation

An ARCH(q) model can be estimated using ordinary least squares. A methodology to test for the lag length of ARCH errors using the Lagrange multiplier test was proposed by Engle (1982). This procedure is as follows:

1. Estimate the best fitting autoregressive model AR(q) $` y_{t} = a_{0} + a_{1} y_{{t-1}} + \cdots + a_{q} y_{{t-q}} + \epsilon_{t} = a_{0} + \sum_{{i=1}}^{q} a_{i} y_{{t-i}} + \epsilon_{t}`$.

2. Obtain the squares of the error $`{\hat \epsilon }^{2}`$ and regress them on a constant and q lagged values: $`{\hat \epsilon }_{t}^{2} = {\hat \alpha }_{0}+\sum _{{i=1}}^{{q}}{\hat \alpha }_{i}{\hat \epsilon }_{{t-i}}^{2}`$, where q is the length of ARCH lags.

3. The null hypothesis is that, in the absence of ARCH components, we have $`\alpha_{i} = 0`$ for all $`i=1,\cdots ,q`$. The alternative hypothesis is that, in the presence of ARCH components, at least one of the estimated $`\alpha _{i}`$ coefficients must be significant. In a sample of T residuals under the null hypothesis of no ARCH errors, the test statistic T'R² follows $`\chi^{2}`$ distribution with q degrees of freedom, where $`T'`$ is the number of equations in the model which fits the residuals vs the lags (i.e. $`T'=T-q`$). If T'R² is greater than the Chi-square table value, we reject the null hypothesis and conclude there is an ARCH effect in the ARMA model. If T'R² is smaller than the Chi-square table value, we do not reject the null hypothesis.

## GARCH (Generalized AutoRegressive Conditional Heteroskedasticity)

### Definition

If an autoregressive moving average (ARMA) model is assumed for the error variance, the model is a generalized autoregressive conditional heteroskedasticity (GARCH) model. 

In that case, the GARCH (p,q) model (where p is the order of the GARCH terms $`\sigma^{2}`$ and q is the order of the ARCH terms $`\epsilon^{2}`$), following the notation of the original paper, is given by

$`y_{t} = x'_{t} b + \epsilon_{t}`$

$`\epsilon_{t}|\psi_{t-1}\sim {\mathcal {N}}(0,\sigma_{t}^{2})`$

$`\sigma_{t}^{2} = \omega +\alpha_{1} \epsilon_{t-1}^{2} + \cdots + \alpha_{q} \epsilon_{t-q}^{2} + \beta_{1} \sigma_{t-1}^{2} + \cdots + \beta_{p} \sigma_{t-p}^{2} = \omega + \sum_{i=1}^{q} \alpha_{i} \epsilon_{t-i}^{2} + \sum_{i=1}^{p} \beta_{i} \sigma_{t-i}^{2}`$

### Modelling

Generally, when testing for heteroskedasticity in econometric models, the best test is the White test. However, when dealing with time series data, this means to test for ARCH and GARCH errors.

Exponentially weighted moving average (EWMA) is an alternative model in a separate class of exponential smoothing models. As an alternative to GARCH modelling it has some attractive properties such as a greater weight upon more recent observations, but also drawbacks such as an arbitrary decay factor that introduces subjectivity into the estimation.
GARCH(p, q) model specification

The lag length p of a GARCH(p,q) process is established in three steps:

1. Estimate the best fitting AR(q) model: $`y_{t}=a_{0}+a_{1}y_{{t-1}} + \cdots + a_{q}y_{{t-q}} + \epsilon _{t}=a_{0} + \sum _{{i=1}}^{q}a_{i}y_{{t-i}}+\epsilon_{t}`$.

2. Compute and plot the autocorrelations of $`\epsilon^{2}`$ by: $`\rho ={{\sum _{{t=i+1}}^{T}({\hat \epsilon }_{t}^{2}-{\hat \sigma }_{t}^{2})({\hat \epsilon }_{{t-1}}^{2}-{\hat \sigma }_{{t-1}}^{2})} \over {\sum _{{t=1}}^{T}({\hat \epsilon }_{t}^{2}-{\hat \sigma }_{t}^{2})^{2}}}`$

3. The asymptotic, that is for large samples, standard deviation of $`\rho(i)`$ is $`1/{\sqrt {T}}`$. Individual values that are larger than this indicate GARCH errors. To estimate the total number of lags, use the Ljung-Box test until the value of these are less than, say, 10% significant. The Ljung-Box Q-statistic follows $`\chi^{2}`$ distribution with n degrees of freedom if the squared residuals $`\epsilon_{t}^{2}`$ are uncorrelated. It is recommended to consider up to T/4 values of n. The null hypothesis states that there are no ARCH or GARCH errors. Rejecting the null thus means that such errors exist in the conditional variance.

## References

[Autoregressive conditional heteroskedasticity](https://en.wikipedia.org/wiki/Autoregressive_conditional_heteroskedasticity)

[Generalised Autoregressive Conditional Heteroskedasticity GARCH(p, q) Models for Time Series Analysis](https://www.quantstart.com/articles/Generalised-Autoregressive-Conditional-Heteroskedasticity-GARCH-p-q-Models-for-Time-Series-Analysis/)
