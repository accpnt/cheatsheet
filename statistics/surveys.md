# Surveys

## Sampling

A sample is a subset of size n from a population of size N selected according to predefined rules. In the simplest case, the n elements of a sample are selected randomly, every element having the same chance of being selected to the sample.

## Population and Sample Quantities 

### Notations

Population: U = {1,2,...,k,...,N}

Variable of interest: Y with individual caracteristics $`Y_k`$. 

Total: $`T_Y = \sum_{k \in U} Y_k`$

Mean: $`\bar{Y} = \frac{T_Y}{N} = \frac{1}{N} \sum_{k \in U} Y_k`$

Variance: $`{\sigma_Y}^2 = \frac{1}{N} \sum_{k \in U} (Y_k - \bar{Y})`$

Dispersion: $`{S_Y}^2 = \frac{1}{N-1} \sum_{k \in U} (Y_k - \bar{Y}) = \frac{N}{N-1} {\sigma_Y}^2`$

### Simple random survey

In the sample s ($`s \in U`$) of size n.

Mean: $`\hat{\bar{y}} = \frac{1}{N-1} \sum_{k \in S} Y_k`$

Empiric dispersion: $`\hat{{s_Y}^2} = \frac{1}{n-1} \sum_{k \in S} (Y_k - \hat{\bar{y}})`$

Order 1 inclusion probability of k: $`\pi_k = \Pr(k \in s) = \sum_{s \in S, k \in s} p(s)`$

Order 2 inclusion probability of k,l : $`\pi_{kl} = \Pr(k \in s, l \in s) = \sum_{s \in S / k,l \in s} p(s)`$

Probability of selecting the sample s: $`p(s) = \frac{1}{{C_N}^n}`$

Probability of selecting the kth individual: $`\forall k \in U, \pi_k = \Pr(k \in S) = \frac{n}{N}`$

#### Estimators of the parameter of interest


## Horvitz-Thompson estimator

## References

[The Horvitz-Thompson Estimator](http://essedunet.nsd.uib.no/cms/topics/weight/3/1.html)
