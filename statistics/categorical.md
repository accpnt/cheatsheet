# Categorical variables

## One categorical variable

## Two categorical variables explained

## One categorical variable explained by one categorical variable

## One quantitative variable explained by one categorical variable

## One categorical variable explained by one quantitative variable

## One categorical variable explained by two categorical variables

## One quantitative variable explained by one quantitative variable

## One quantitative variable explained by one quantitative variable and one categorical variable

## One categorical variable explained by two quantitative variables

## Several categorical variables explained

## AFC

## Decision trees

## PLS-DA
