# Repeated measures

## Repeated Measures ANOVA

### Introduction

Repeated measures ANOVA is a test that seems close to one-way ANOVA as it allows to check for differences between the means of three and more groups. The essential difference is that the groups are dependent (i.e. related). This means that the groups contains data or measurements from the same individuals. What differs in terms of design may be when measurements were taken (measurements were thus repeated in time: before, during and after an experiment OR every day during a given time frame, OR etc) or in which circumstances or conditions measurements were performed (for example, measurements were done 3 times, each time following application of a new drug). “As usual”, the null hypothesis states that the means of all groups are equal.

### Assumptions

* each individual is represented in the form of a measurement in each of the tested groups (there cannot be any missing value),
* normality of distribution
* sphericity, which means that equality of variance is verified when comparing any two groups (all possible pairs of groups) in the experimental design
* groups do not contain outliers.

### References

[Repeated Measures ANOVA](https://biostats.w.uib.no/repeated-measures-anova/)

[Two-Way ANOVA with Repeated Measures](https://datascienceplus.com/two-way-anova-with-repeated-measures/)

[Six Differences Between Repeated Measures ANOVA and Linear Mixed Models](https://www.theanalysisfactor.com/six-differences-between-repeated-measures-anova-and-linear-mixed-models/)
