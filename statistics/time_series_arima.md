# Time Series (ARIMA)

## Concepts

### Time series

$`x_t = m_t + s_t + z_t`$

where, 

* $`x_t`$ : Variable measured at time t
* $`m_t`$ : Trend
* $`s_t`$ : Seasonality
* $`z_t`$ : Noise

Is the variance stationary ?

### Non-stationary variance

Just like the mean you can attempt to remove the non-stationarity in variance. However, to remove non-stationary variance you divide by an estimate of the standard deviation. 

### Sample autocovariance

The sample autocovariance of $`x_t`$ at lag h, is 

$`\hat{\gamma}(h) = \frac{1}{n} \sum_{t=1}^{n-h} (x_{t+h} - \bar{x}) (x_t - \bar{x})`$

Almost the usual deﬁnition of sample covariance, apart from divisor.

### Sample autocorrelation

The sample autocorrelation of $`x_t`$ at lag h, is the sample covariance at lag h, divided by the sample covariance at lag 0, 

$`\hat{\rho}(h) = \frac{\hat{\gamma}(h)}{\hat{\gamma}(0)}`$

Usually displayed at a plot against h. aka Correlogram

### Autocorrelation function

The autocorrelation function and it's partner the partial autocorrelation function are central to time domain time series analysis. 

### White noise

$`{w_t}`$ is a white noise process if $`w_t`$ are uncorrelated identically distributed random variables with $`E[w_t]= 0`$  and $`Var[w_t]= \sigma^2`$ , for all t. 

If the $`w_t`$ are Normally (Gaussian) distributed, the series is known as Gaussian white noise.

What is the mean function ? 

$`\mu_t = E[w_t]= 0`$ 

What is the autocovariance function? 

$`\gamma(h) = \sigma^2`$ for h = 1 ; 0,  otherwise 

Is white noise stationary? Yes.

## ARMA

### General Linear Process

A linear process xt is deﬁned to be a linear combination of white noise variates, Zt, 

$`x_{i} = \sum_{i=0}^{\infty} \psi_{i} Z_{t-1}`$

with 

$`\sum_{i=0}^{\infty} \left|\psi_{i}\right| < \infty`$

This is enough to ensure stationarity. 

One can show that the autocovariance of a linear process is, 

$`\eta(h) =  \sigma^2  \sum_{i=0}^{\infty} \psi_{i+h} \psi_{i}`$

### MA(q) process

A moving average model of order q is deﬁned to be, 

$`x_t = Z_t + \beta_{1} Z_{t-1} + \beta_{2} Z_{t-2} + ... + \beta_{q} Z_{t-q}`$

where Zt is a white noise process with variance σ2, and the β1,..., βq are parameters.

The moving average operator is, 

$`\theta(B) = 1 + \beta_{1} B + \beta_{2} B^2 + ... + \beta_{q} B^q`$

### AR(p) process

An autoregressive process of order p is deﬁned to be, 

$`x_t = \alpha_{1} x_{t-1} + \alpha_{2} x_{t-2} + ... + \alpha_{q} x_{t-q} + Z_t`$

where Zt is a white noise process with variance $`\sigma^2`$, and the $`\alpha_{i}`$ are parameters.

$`\phi(B) = 1 - \alpha_{1} B - \alpha_{2} B^2 - ... - \alpha_{p} B^p`$

### Three stationary models

White noise

* $`\rho(h) = 1`$ when h=0
* $`\rho(h) = 0`$ otherwise
* Only lag 0 shows non-zero ACF

MA(1), any $`\beta_1`$

* $`\rho(h) = 1`$, when h=0
* $`\rho(h) = \frac{\beta_1}{1 + {\beta_1}^2}`$, when h=1
* $`\rho(h) = 0`$, when $`2 \leq h`$
* Only lag 0 and 1 show non-zero ACF

AR(1), $`|\alpha_1| < 1`$

* $`\rho(h) = 1`$, when h=0
* $`\rho(h) = {\alpha_1}^h`$, when h > 0
* Decreasing ACF


### Behavior of the ACF and PACF for ARMA models

| | AR(p) | MA(q) | ARMA(p,q) |
|-|-------|-------|-----------|
| ACF | tails off | cuts off after lag q | tails off |
| PACF | cuts off after lag p | tails off | tails off |

* cuts off means it becomes 0 abruptly
* tails off means that it decays to 0 asymptotically  

### ARMA(p,q) process

A process, $`x_t`$, is ARMA(p,q) if it has the form, $`\phi(B) x_t = \theta(B) Z_t`$, where $`Z_t`$ is a white noise process with variance $`\sigma^2`$, and We will assume $`Z_t`$ ~ N(0, $`\sigma^2`$).

## ARIMA 

### Definition

A process $`x_t`$ is ARIMA(p, d, q) if $`x_t`$ differenced d times ($`\nabla^d x_t`$) is an ARMA(p, q) process. 

I.e. $`x_t`$ is deﬁned by 

$`\theta(B) \nabla^d x_t = \theta(B) w_t`$

$`\theta(B) (1 - B)^d x_t = \theta(B) w_t`$

ARIMA (1,0,0) is AR(1), ARIMA(0,1,0) is I(1), and ARIMA(0,0,1) is MA(1).

### Procedure for ARIMA modeling

1. Plot the data.  Transform? Outliers? Differencing ? 
2. Difference until series is stationary, i.e. ﬁnd d. 
3. Examine differenced series and pick p and q. 
4. Fit ARIMA(p, d, q) model to original data. 
5. Check model diagnostics 
6. Forecast (back transform?)

### Checking model diagnosis

* Linearly decreasing ACF, common sign of presence of trend, try differencing!

### SARIMA models

The idea is very similar, if one seasonal cycle lasts for s measurements, then if we difference at lag s,  

$`y_t = \nabla_s x_t = x_t - x_{t-s} = (1 - B^s) x_t`$  

we will remove the seasonality.  

Differencing seasonally D times is denoted, 

$`\nabla^D_s x_t = (1 - B^s)^D x_t`$ 

A multiplicative seasonal autoregressive integrated moving average model, SARIMA(p, d, q) x (P, D, Q)s is given by 

$`\theta(B^s) \theta(B) \nabla^D_s \nabla^d x_t = \theta(B^s) \theta(B) w_t`$

Have to specify s, then choose p, d, q, P, D and Q

$`\nabla^D_s \nabla^d x_t`$ just an ARMA model with lots of coefﬁcients set to zero.

### Procedure for SARIMA modeling

1. Plot the data.  Transform? Outliers? Differencing? 
2. Difference to remove trend, ﬁnd d. Then difference to remove seasonality, ﬁnd D. 
3. Examine acf and pacf of differenced series.  Find P and Q ﬁrst, by examining just at lags s, 2s, 3s, etc.  Find p and q by examining between seasonal lags. 
4. Fit SARIMA(p, d, q)x(P, D, Q)s model to original data. 
5. Check model diagnostics 
6. Forecast (back transform?)

## References

[Stat 565 - Time Series](http://stat565.cwick.co.nz/)

[Interpret the partial autocorrelation function (PACF)](https://support.minitab.com/en-us/minitab/18/help-and-how-to/modeling-statistics/time-series/how-to/partial-autocorrelation/interpret-the-results/partial-autocorrelation-function-pacf/)

[Identifying the numbers of AR or MA terms in an ARIMA model](http://people.duke.edu/~rnau/411arim3.htm)
