# Logistic regression

## Overview

In regression analysis, logistic regression (or logit regression) is estimating the parameters of a logistic model (a form of binary regression). Mathematically, a binary logistic model has a dependent variable with two possible values, such as pass/fail, win/lose, alive/dead or healthy/sick; these are represented by an indicator variable, where the two values are labeled "0" and "1". 

In the logistic model, the log-odds (the logarithm of the odds) for the value labeled "1" is a linear combination of one or more independent variables ("predictors"); the independent variables can each be a binary variable (two classes, coded by an indicator variable) or a continuous variable (any real value). 

The corresponding probability of the value labeled "1" can vary between 0 (certainly the value "0") and 1 (certainly the value "1"), hence the labeling; the function that converts log-odds to probability is the logistic function, hence the name. The unit of measurement for the log-odds scale is called a logit, from logistic unit, hence the alternative names. 

Analogous models with a different sigmoid function instead of the logistic function can also be used, such as the probit model; the defining characteristic of the logistic model is that increasing one of the independent variables multiplicatively scales the odds of the given outcome at a constant rate, with each dependent variable having its own parameter; for a binary independent variable this generalizes the odds ratio. 

## Asumptions

The logistic regression method assumes that:

* The outcome is a binary or dichotomous variable like yes vs no, positive vs negative, 1 vs 0.
* There is a linear relationship between the logit of the outcome and each predictor variables. Recall that the logit function is logit(p) = log(p/(1-p)), where p is the probabilities of the outcome.
* There is no influential values (extreme values or outliers) in the continuous predictors
* There is no high intercorrelations (i.e. multicollinearity) among the predictors.

To improve the accuracy of your model, you should make sure that these assumptions hold true for your data. 

## Interpret the key results for Binary Logistic Regression

### Step 1: Determine whether the association between the response and the term is statistically significant

To determine whether the association between the response and each term in the model is statistically significant, compare the p-value for the term to your significance level to assess the null hypothesis. The null hypothesis is that the term's coefficient is equal to zero, which indicates that there is no association between the term and the response. Usually, a significance level (denoted as $`\alpha`$ or alpha) of 0.05 works well. A significance level of 0.05 indicates a 5% risk of concluding that an association exists when there is no actual association.

* P-value $`\leq \alpha`$: The association is statistically significant
    * If the p-value is less than or equal to the significance level, you can conclude that there is a statistically significant association between the response variable and the term. 
* P-value > $`\alpha`$: The association is not statistically significant
    * If the p-value is greater than the significance level, you cannot conclude that there is a statistically significant association between the response variable and the term. You may want to refit the model without the term. 
    * If there are multiple predictors without a statistically significant association with the response, you must reduce the model by removing terms one at a time. 

If a model term is statistically significant, the interpretation depends on the type of term. The interpretations are as follows:

* If a continuous predictor is significant, you can conclude that the coefficient for the predictor does not equal zero.
* If a categorical predictor is significant, you can conclude that not all the level means are equal.

### Step 2: Understand the effects of the predictors

Use the odds ratio to understand the effect of a predictor.

#### Odds Ratios for Continuous Predictors
    
Odds ratios that are greater than 1 indicate that the even is more likely to occur as the predictor increases. Odds ratios that are less than 1 indicate that the event is less likely to occur as the predictor increases.
     
#### Odds Ratios for Categorical Predictors

For categorical predictors, the odds ratio compares the odds of the event occurring at 2 different levels of the predictor. Odds ratios that are greater than 1 indicate that the event is more likely at level A. Odds ratios that are less than 1 indicate that the event is less likely at level A. 

### Step 3: Determine how well the model fits your data

To determine how well the model fits your data, examine the statistics in the Model Summary table. For binary logistic regression, the data format affects the deviance R2 statistics but not the AIC. For more information, go to For more information, go to How data formats affect goodness-of-fit in binary logistic regression.

#### Deviance R-sq

The higher the deviance R2, the better the model fits your data. Deviance R2 is always between 0% and 100%.

Deviance R2 always increases when you add additional predictors to a model. For example, the best 5-predictor model will always have an R2 that is at least as high as the best 4-predictor model. Therefore, deviance R2 is most useful when you compare models of the same size.

For binary logistic regression, the format of the data affects the deviance R2 value. The deviance R2 is usually higher for data in Event/Trial format. Deviance R2 values are comparable only between models that use the same data format.

Deviance R2 is just one measure of how well the model fits the data. Even when a model has a high R2, you should check the residual plots to assess how well the model fits the data.

#### Deviance R-sq (adj)

Use adjusted deviance R2 to compare models that have different numbers of predictors. Deviance R2 always increases when you add a predictor to the model. The adjusted deviance R2 value incorporates the number of predictors in the model to help you choose the correct model.

#### AIC

Use AIC to compare different models. The smaller the AIC, the better the model fits the data. However, the model with the smallest AIC does not necessarily fit the data well. Also use the residual plots to assess how well the model fits the data. 

### Step 4: Determine whether the model does not fit the data

Use the goodness-of-fit tests to determine whether the predicted probabilities deviate from the observed probabilities in a way that the binomial distribution does not predict. If the p-value for the goodness-of-fit test is lower than your chosen significance level, the predicted probabilities deviate from the observed probabilities in a way that the binomial distribution does not predict. This list provides common reasons for the deviation:

* Incorrect link function
* Omitted higher-order term for variables in the model
* Omitted predictor that is not in the model
* Overdispersion

If the deviation is statistically significant, you can try a different link function or change the terms in the model.

For binary logistic regression, the format of the data affects the p-value because it changes the number of trials per row.

* Deviance: The p-value for the deviance test tends to be lower for data that are in the Binary Response/Frequency format compared to data in the Event/Trial format. For data in Binary Response/Frequency format, the Hosmer-Lemeshow results are more trustworthy.
* Pearson: The approximation to the chi-square distribution that the Pearson test uses is inaccurate when the expected number of events per row in the data is small. Thus, the Pearson goodness-of-fit test is inaccurate when the data are in Binary Response/Frequency format.
* Hosmer-Lemeshow: The Hosmer-Lemeshow test does not depend on the number of trials per row in the data as the other goodness-of-fit tests do. When the data have few trials per row, the Hosmer-Lemeshow test is a more trustworthy indicator of how well the model fits the data.

## Multinomial logistic regression

Multinomial logistic regression (often just called 'multinomial regression') is used to predict a nominal dependent variable given one or more independent variables. It is sometimes considered an extension of binomial logistic regression to allow for a dependent variable with more than two categories. As with other types of regression, multinomial logistic regression can have nominal and/or continuous independent variables and can have interactions between independent variables to predict the dependent variable.

## Linear vs Logistic regression

![](http://res.cloudinary.com/dyd911kmh/image/upload/f_auto,q_auto:best/v1523361626/linear_vs_logistic_regression_h8voek.jpg)

## Exemple

Let us try to understand logistic regression by considering a logistic model with given parameters, then seeing how the coefficients can be estimated from data. Consider a model with two predictors, x1 and x2, and one binary (Bernoulli) response variable Y, for which we denote p = P (Y = 1) = E (Y). Predictors may be continuous variables (taking a real number as value), or binary variables (taking value 0 or 1). Then the general form of the log-odds (here denoted by $`\ell`$) is:

$`\ell =\log{\frac{p}{1-p}} = \beta_{0} + \beta_{1}x_{1} + \beta_{2}x_{2}`$

The log odds are the logarithm of the odds (i.e. the ratio between a probability value and its complementary).

The coefficients $`\beta_{i}`$ are the parameters of the model. Note that this is a linear model: the log-odds $`\ell`$ are a linear combination of the predictors x1 and x2, including a constant term $`\beta_{0}`$. The corresponding odds are the exponential:

$`o = b^{\beta_{0}+\beta_{1}x_{1}+\beta_{2}x_{2}}`$

where b is the base of the logarithm. This is now a non-linear model since the odds are not a linear combination of the predictors.

By simple algebraic manipulation, the corresponding probability is:

$`p={\frac{b^{\beta_{0} + \beta_{1}x_{1} + \beta_{2}x_{2}}}{b^{\beta_{0} + \beta_{1}x_{1} + \beta _{2}x_{2}} + 1}} = {\frac{1}{1 + b^{-(\beta_{0} + \beta_{1}x_{1}+\beta_{2}x_{2})}}}`$

The base b is usually taken to be e, but for exposition we here use 10, so the odds are more understandable. Suppose the coefficients are -3,1,2, so the model is:

$`\ell =-3 + 1\cdot x_{1} + 2\cdot x_{2}`$

This can be interpreted as follows:

* $`\beta_{0} = -3`$ is the y-intercept, and can thus be interpreted as the log-odds when the predictors are all zero; here the log-odds are -3, so the odds are 10 - 3 = 10 - 3 : 1 = 1 : 1000, and the probability is $`1/(1000+1)=1/1001\approx 0.001=0.1\%`$.
      * Alternatively, grouping the weighted predictors as a single contribution to the total logit, $`\ell '=1\cdot x_{1}+2\cdot x_{2}`$, so $`\ell = \ell' - 3`$, the quantity $`-\beta_{0}`$ is the x-intercept, and can be interpreted as the weighted predictors corresponding to even odds: if $`\ell '=1\cdot x_{1}+2\cdot x_{2} = 3`$, then the log-odds are 0, the odds for are $`10^{0}=10^{0}:1=1:1`$, and the probability is $`1/(1+1)=1/2=50\%`$.
* $`\beta_{1}=1`$ means that increasing x1 by 1 increases the log-odds by 1 . 1, so it multiplies the odds by $` 10^{1}=10`$; this is sometimes referred to as the "effect" of the predictor x1.
* $`\beta_{2} = 2`$ means that increasing x2 by 1 increases the log-odds by 2 . 1, so it multiplies the odds by $`10^{2}=100`$. Thus the effect of x2} on the log-odds is twice as great as the effect of x1.

Such a model can be used for various purposes. For example, given an individual datum with values of predictors x1, x2, one can estimate the log-odds (hence odds and probability) of the outcome by putting in the values in the formula. Alternatively, consider comparing two medical treatments, one of which decreases x1 by 3 and the other that decreases x2 by 2. Assuming this model is valid, the first treatment reduces the log-odds of the outcome by 1 . 3 = 3, so it reduces the odds by a factor of $`10^{3}=1000`$, but the other treatment reduces the log-odds by 2 . 2 = 4 , so it reduces the odds by a factor of $`10^{4}=10000`$, and thus is more effective, all else equal.

In order to estimate the parameters of such a logistic model and compute how well it fits the data, one must do logistic regression.

## References

[Interpret the key results for Binary Logistic Regression](https://support.minitab.com/en-us/minitab-express/1/help-and-how-to/modeling-statistics/regression/how-to/binary-logistic-regression/interpret-the-results/key-results/)

[Logistic regression](https://en.wikipedia.org/wiki/Logistic_regression)

[ML Cheatsheet - Logistic Regression](https://ml-cheatsheet.readthedocs.io/en/latest/logistic_regression.html)

[Logistic Regression Assumptions and Diagnostics in R](http://www.sthda.com/english/articles/36-classification-methods-essentials/148-logistic-regression-assumptions-and-diagnostics-in-r/#logistic-regression-assumptions)

[Multinomial Logistic Regression using SPSS Statistics](https://statistics.laerd.com/spss-tutorials/multinomial-logistic-regression-using-spss-statistics.php)

[Multinomial Logistic Regression | R Data Analysis Examples](https://stats.idre.ucla.edu/r/dae/multinomial-logistic-regression/)
