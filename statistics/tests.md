# Statistical tests

![Statistical test Cheat Sheet](assets/tests_cheatsheet.jpg)

## Overview 

* Normality Tests
    * Shapiro-Wilk Test
    * D’Agostino’s K^2 Test
    * Anderson-Darling Test
* Correlation Tests
    * Pearson’s Correlation Coefficient
    * Spearman’s Rank Correlation
    * Kendall’s Rank Correlation
    * Chi-Squared Test
* Parametric Statistical Hypothesis Tests
    * Likelihood Ratio Test
    * Wald test
    * Student’s t-test
    * Paired Student’s t-test
    * Analysis of Variance Test (ANOVA)
    * Repeated Measures ANOVA Test
    * Durbin-Watson test
* Nonparametric Statistical Hypothesis Tests
    * Mann-Whitney U Test
    * Wilcoxon Signed-Rank Test
    * Kruskal-Wallis H Test
    * Friedman Test

## Tests

### Normality

#### Shapiro-Wilk Test

Tests whether a data sample has a Gaussian distribution.

Assumptions

* Observations in each sample are independent and identically distributed (iid).

Interpretation

* H0: the sample has a Gaussian distribution.
* H1: the sample does not have a Gaussian distribution.

#### D’Agostino’s K^2 Test

Tests whether a data sample has a Gaussian distribution.

Assumptions

* Observations in each sample are independent and identically distributed (iid).

Interpretation

* H0: the sample has a Gaussian distribution.
* H1: the sample does not have a Gaussian distribution.

#### Anderson-Darling Test

Tests whether a data sample has a Gaussian distribution.

Assumptions

* Observations in each sample are independent and identically distributed (iid).

Interpretation

* H0: the sample has a Gaussian distribution.
* H1: the sample does not have a Gaussian distribution.

### Correlation

#### Pearson’s Correlation Coefficient

Tests whether two samples have a linear relationship.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample are normally distributed.
* Observations in each sample have the same variance.

Interpretation

* H0: the two samples are independent.
* H1: there is a dependency between the samples.

#### Spearman’s Rank Correlation

Tests whether two samples have a monotonic relationship.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample can be ranked.

Interpretation

* H0: the two samples are independent.
* H1: there is a dependency between the samples.

#### Kendall’s Rank Correlation

Tests whether two samples have a monotonic relationship.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample can be ranked.

Interpretation

* H0: the two samples are independent.
* H1: there is a dependency between the samples.

#### Chi-Squared Test

Tests whether two categorical variables are related or independent.

Assumptions

* Observations used in the calculation of the contingency table are independent.
* 25 or more examples in each cell of the contingency table.

Interpretation

* H0: the two samples are independent.
* H1: there is a dependency between the samples.

### Parametric Statistical Hypothesis Tests

#### Likelihood Ratio Test

The likelihood ratio test is a test of the sufficiency of a smaller model versus a more complex model. The null hypothesis of the test states that the smaller model provides as good a fit for the data as the larger model. If the null hypothesis is rejected, then the alternative, larger model provides a significant improvement over the smaller model.

To use the likelihood ratio test, the null hypothesis model must be a model nested within, that is, a special case of, the alternative hypothesis model. For example, the scaled identity structure is a special case of the compound symmetry structure, and compound symmetry is a special case of the unstructured matrix. However, the autoregressive and compound symmetry structures are not special cases of each other.

The likelihood ratio test can be used to test repeated effect or random effect covariance structures, or both at the same time. For example, it is possible to test a model that has an identity structure for a random effect and an autoregressive structure for the repeated effect, versus a model that has a compound symmetry structure for the random effect and an unstructured matrix for the repeated effect. Simply make sure that the covariance structure for each effect in one model is nested within the covariance structures for the effects in the other model. 

* H0: the smaller model provides as good a fit for the data as the larger model. 
* H1: the larger model provides a significant improvement over the smaller model. 

If H0 is true, $`LRT ∼ \chi^2 (df=k-1)`$

#### Wald test

The Wald test approximates the lr test, but with the advantage that it only requires estimating one model. The Wald test works by testing the null hypothesis that a set of parameters is equal to some value. In the model being tested here, the null hypothesis is that the two coefficients of interest are simultaneously equal to zero. If the test fails to reject the null hypothesis, this suggests that removing the variables from the model will not substantially harm the fit of that model, since a predictor with a coefficient that is very small relative to its standard error is generally not doing much to help predict the dependent variable. The formula for a Wald test is a bit more daunting than the formula for the lr test, so we won’t write it out here (see Fox, 1997, p. 569, or other regression texts if you are interested). To give you an intuition about how the test works, it tests how far the estimated parameters are from zero (or any other value under the null hypothesis) in standard errors, similar to the hypothesis tests typically printed in regression output. The difference is that the Wald test can be used to test multiple parameters simultaneously, while the tests typically printed in regression output only test one parameter at a time.

If the p-value is less than the generally used criterion of 0.05, we are able to reject the null hypothesis, indicating that the coefficients are not simultaneously equal to zero.

#### Student’s t-test

Tests whether the means of two independent samples are significantly different.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample are normally distributed.
* Observations in each sample have the same variance.

Interpretation

* H0: the means of the samples are equal.
* H1: the means of the samples are unequal.

#### Paired Student’s t-test

Tests whether the means of two paired samples are significantly different.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample are normally distributed.
* Observations in each sample have the same variance.
* Observations across each sample are paired.

Interpretation

* H0: the means of the samples are equal.
* H1: the means of the samples are unequal.

#### Analysis of Variance Test (ANOVA)

Tests whether the means of two or more independent samples are significantly different.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample are normally distributed.
* Observations in each sample have the same variance.

Interpretation

* H0: the means of the samples are equal.
* H1: one or more of the means of the samples are unequal.

#### Repeated Measures ANOVA Test

Tests whether the means of two or more paired samples are significantly different.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample are normally distributed.
* Observations in each sample have the same variance.
* Observations across each sample are paired.

Interpretation

* H0: the means of the samples are equal.
* H1: one or more of the means of the samples are unequal.

#### Durbin-Watson test

Durbin–Watson statistic is a test statistic used to detect the presence of autocorrelation at lag 1 in the residuals (prediction errors) from a regression analysis. 

Assumptions

* Autoregressive errors on the error part of a linear regression model. If $`Y = \beta X + \epsilon`$, then $`\epsilon_{t} = \rho \epsilon_{t-1} + u_t`$ with $`|\rho| < 1`$. Durbin-Watson test is a parametric test on $`\rho`$. 

Interpretation

* H0: $`\rho = 0`$, the null hypothesis that linear regression residuals are uncorrelated. 
* H1: $`\rho =\ne 0`$, the alternative hypothesis that linear regression residuals are correlated

### Nonparametric Statistical Hypothesis Tests

#### Mann-Whitney U Test

Tests whether the distributions of two independent samples are equal or not.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample can be ranked.

Interpretation

* H0: the distributions of both samples are equal.
* H1: the distributions of both samples are not equal.

#### Wilcoxon Signed-Rank Test

Tests whether the distributions of two paired samples are equal or not.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample can be ranked.
* Observations across each sample are paired.

Interpretation

* H0: the distributions of both samples are equal.
* H1: the distributions of both samples are not equal.

#### Kruskal-Wallis H Test

Tests whether the distributions of two or more independent samples are equal or not.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample can be ranked.

Interpretation

* H0: the distributions of all samples are equal.
* H1: the distributions of one or more samples are not equal.

#### Friedman Test

Tests whether the distributions of two or more paired samples are equal or not.

Assumptions

* Observations in each sample are independent and identically distributed (iid).
* Observations in each sample can be ranked.
* Observations across each sample are paired.

Interpretation

* H0: the distributions of all samples are equal.
* H1: the distributions of one or more samples are not equal.

## References

[The Statistics Tutor's Quick Guide to Commonly Used Statistical Tests](http://www.statstutor.ac.uk/resources/uploaded/tutorsquickguidetostatistics.pdf)

[15 Statistical Hypothesis Tests in Python (Cheat Sheet)](https://machinelearningmastery.com/statistical-hypothesis-tests-in-python-cheat-sheet/)

[Statistical test Cheat Sheet](https://dacg.in/2018/11/17/statistical-test-cheat-sheet/)

[Likelihood Ratio Test](https://www.ibm.com/support/knowledgecenter/en/SSLVMB_23.0.0/spss/tutorials/mixed_diet_intro_04.html)

[FAQ:How are the likelihood ratio, Wald, and Lagrange multiplier (score) tests different and/or similar?](https://stats.idre.ucla.edu/other/mult-pkg/faq/general/faqhow-are-the-likelihood-ratio-wald-and-lagrange-multiplier-score-tests-different-andor-similar/)

[What is the meaning of p values and t values in statistical tests?](https://stats.stackexchange.com/questions/31/what-is-the-meaning-of-p-values-and-t-values-in-statistical-tests)

[What a p-value tells you about statistical significance](https://www.simplypsychology.org/p-value.html)
