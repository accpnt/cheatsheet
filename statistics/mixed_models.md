# Mixed Models

## Introduction

A mixed model (or more precisely mixed error-component model) is a statistical model containing both fixed effects and random effects. They are particularly useful in settings where repeated measurements are made on the same statistical units (longitudinal study), or where measurements are made on clusters of related statistical units. Because of their advantage in dealing with missing values, mixed effects models are often preferred over more traditional approaches such as repeated measures ANOVA. 

## Definition

In matrix notation a mixed model can be represented as

$`y = X \beta + Z u + \epsilon`$

where

* y is a known vector of observations, with mean E(y) = X $`\beta`$
* $`\beta`$ is an unknown vector of fixed effects;
* u is an unknown vector of random effects, with mean E (u) = 0 and variance–covariance matrix var(u) = G
* $`\epsilon`$ is an unknown vector of random errors, with mean E($`\epsilon`$) = 0 and variance var($`\epsilon`$) = R
* X and Z are known design matrices relating the observations y to $`\beta`$ and u, respectively.

## References

[Introduction to linear mixed models](https://ourcodingclub.github.io/2017/03/15/mixed-models.html)

[Introduction to Linear Mixed Models](https://stats.idre.ucla.edu/other/mult-pkg/introduction-to-linear-mixed-models/)

[Understanding Random Effects in Mixed Models](https://www.theanalysisfactor.com/understanding-random-effects-in-mixed-models/)

[A very basic tutorial for performing linear mixed effects](http://www.bodowinter.com/tutorial/bw_LME_tutorial2.pdf)

[How do I report and interpret the output from linear mixed models with interaction terms?](https://stats.stackexchange.com/questions/266094/how-do-i-report-and-interpret-the-output-from-linear-mixed-models-with-interacti)

[A Practical Guide to Mixed Models in R](https://ase.tufts.edu/gsc/gradresources/guidetomixedmodelsinr/mixed%20model%20guide.html)