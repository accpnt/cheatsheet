# ANOVA

## Foreword

ANOVA is used to compare differences of means among more than 2 groups. It does this by looking at variation in the data and where that variation is found (hence its name). Specifically, ANOVA compares the amount of variation between groups with the amount of variation within groups. It can be used for both observational and experimental studies.

When we take samples from a population, we expect each sample mean to differ simply because we are taking a sample rather than measuring the whole population; this is called sampling error but is often referred to more informally as the effects of “chance”. Thus, we always expect there to be some differences in means among different groups. The question is: is the difference among groups greater than that expected to be caused by chance? In other words, is there likely to be a true (real) difference in the population mean?

Although it may seem difficult at first, statistics becomes much easier if you understand what the test is doing rather than blindly applying it. Hopefully ANOVA will become clear by following the steps below.

## The ANOVA model

Mathematically, ANOVA can be written as:

$`x_{ij} = \mu_i + \epsilon_{ij}`$

where x are the individual data points (i and j denote the group and the individual observation), $`\epsilon`$ is the unexplained variation and the parameters of the model ($`\mu`$) are the population means of each group. Thus, each data point ($`x_{ij}`$) is its group mean plus error.

## Assumptions

* the response is normally distributed
* variance is similar within different groups
* the data points are independent

## Hypothesis testing

Like other classical statistical tests, we use ANOVA to calculate a test statistic (the F-ratio) with which we can obtain the probability (the p-value) of obtaining the data assuming the null hypothesis. A significant p-value (usually taken as P < 0.05) suggests that at least one group mean is significantly different from the others.

Null hypothesis: all population means are equal

Alternative hypothesis: at least one population mean is different from the rest.

## Calculation of the F ratio

ANOVA separates the variation in the dataset into 2 parts: between-group and within-group. These variations are called the sums of squares, which can be seen in the equations below. 

* N: number of observations
* C: number of modalities of a factor

### Step 1: variation between groups

The between-group variation (or between-group sums of squares, SS) is calculated by comparing the mean of each group with the overall mean of the data.

$`\alpha_i = \bar{X_i} - \bar{X}`$

with $`\bar{X_i} = \sum_{j=1}^{N} \frac{X_{ij}}{n_i}`$ 

where:

* N is the total number of observations
* $`n_i`$ is the number of observations for the ith modality
* $`\bar{X_i}`$ is the mean for a modality

$`\bar{X} = \sum_{i=1}^{C} \sum_{j=1}^{N} \frac{X_{ij}}{N_{total}}`$

with $`N_{total} = \sum_{i=1}^{C} n_i`$

### Step 2: variation within groups

The within-group variation (or the within-group sums of squares) is the variation of each observation from its group mean.

$`X_{ij} - \bar{X} = (\bar{X_j} - \bar{X}) + (X_{ij} - \bar{X_j})`$

$`\sum_{i=1}^{C} \sum_{j=1}^{N} (X_{ij} - \bar{X})^2 = \sum_{i=1}^{C} \sum_{j=1}^{n_j} (X_{i} - \bar{X})^2 + \sum_{i=1}^{C} \sum_{j=1}^{N} (X_{ij} - \bar{X_j})^2`$

SST = SSE + SSR

* SST: Square Sum Total, expresses total variability of observations
* SSE: Square Sum Explained, expresses the explained variability (the variation explained by the factor)
* SSR: Square Sum Residual, expresses the residual variability (the variation not explained by the factor)

### Step 3: the F ratio

The F-ratio is then calculated as: 

$`\frac{\frac{SSE}{C - 1}}{\frac{SSR}{N-C}}`$

## Degrees of freedom

The F-ratio follows a Fisher law: F(C-1, N-C). 

## References

[Research Point: ANOVA Explained](https://en-author-services.edanzgroup.com/blogs/statistics-anova-explained)

[The Analysis of Variance (ANOVA) table and the F-test](https://newonlinecourses.science.psu.edu/stat462/node/107/)

[The Basic Idea of an Analysis of Variance (ANOVA)](https://www.dummies.com/education/science/biology/the-basic-idea-of-an-analysis-of-variance-anova/)
