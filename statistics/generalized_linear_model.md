# Generalized linear model (GzLM)

## Overview

In a generalized linear model (GzLM), each outcome Y of the dependent variables is assumed to be generated from a particular distribution in an exponential family, a large class of probability distributions that includes the normal, binomial, Poisson and gamma distributions, among others. The mean, $`\mu`$, of the distribution depends on the independent variables, X, through:

$`E(Y) = \mu = g^{-1}(X \beta)`$

where:

* E(Y) is the expected value of Y
* $`X \beta`$ is the linear predictor, a linear combination of unknown parameters $`\beta`$
* g is the link function.

In this framework, the variance is typically a function, V, of the mean:

$`Var(Y) = V(\mu)`$

It is convenient if V follows from an exponential family of distributions, but it may simply be that the variance is a function of the predicted value.

The unknown parameters, $`\beta`$, are typically estimated with maximum likelihood, maximum quasi-likelihood, or Bayesian techniques. 

## Model components

A generalized linear model (or GLM) consists of three components:

1. A random component, specifying the conditional distribution of the response variable, Yi (for the ith of n independently sampled observations), given the values of the explanatory variables in the model. In the initial formulation of GLMs, the distribution of Yi was a member of an exponential family, such as the Gaussian, binomial, Poisson, gamma, or inverse-Gaussian families of distributions.
2. A linear predictor—that is a linear function of regressors, $`\eta_i = \alpha + \beta_{i1} X_{i1} + \beta_{i2} X_{i2} + .. + \beta_{ik} X_{ik}`$. 
3. A smooth and invertible linearizing link function g(·), which transforms the expectation of the response variable, $`\mu_i = E(Y_i)`$, to the linear predictor: $`g(\mu_i) = \eta_i`$. 

Summarized, the GzLM consists of three elements:

1. An exponential family of probability distributions.
2. A linear predictor $`\eta = X \beta`$.
3. A link function g such that $`E(Y|X) = \mu = g^{-1}(\eta)`$.

So we have: $`X \beta = g(\mu)`$.

## Link functions

| Distribution                                  | Link name        | Link function                       | Mean function                                       |
|-----------------------------------------------|------------------|-------------------------------------|-----------------------------------------------------|
| Normal                                        | Identity         | $`X \beta = \mu`$                   | $`\mu = X \beta`$                                   |
| Exponential, gamma                            | Negative inverse | $`X \beta = -\mu^{-1}`$             | $`\mu = -(X \beta)^{-1}`$                           |
| Inverse gaussian                              | Inverse  squared | $`X \beta = \mu^{-2}`$              | $`\mu = (X \beta)^{-1/2}`$                          |
| Poisson                                       | Log              | $`X \beta = ln(\mu)`$               | $`\mu = exp(X \beta)`$                              |
| Bernoulli, Binomial, Categorical, Multinomial | Logit            | $`X \beta = ln(\frac{\mu}{1-\mu})`$ | $`\mu = \frac{exp(X \beta)}{1 - exp(X \beta)}`$     |

## Generalized additive model (GAM)

In statistics, a generalized additive model (GAM) is a generalized linear model in which the linear predictor depends linearly on unknown smooth functions of some predictor variables, and interest focuses on inference about these smooth functions. GAMs were originally developed by Trevor Hastie and Robert Tibshirani to blend properties of generalized linear models with additive models.

The model relates a univariate response variable, Y, to some predictor variables, xi. An exponential family distribution is specified for Y (for example normal, binomial or Poisson distributions) along with a link function g (for example the identity or log functions) relating the expected value of Y to the predictor variables via a structure such as

$`g(E(Y)) = \beta_0 + f_1(x_1) + f_2(x_2) + .. + f_m(x_m)`$

The functions fi may be functions with a specified parametric form (for example a polynomial, or an un-penalized regression spline of a variable) or may be specified non-parametrically, or semi-parametrically, simply as 'smooth functions', to be estimated by non-parametric means. So a typical GAM might use a scatterplot smoothing function, such as a locally weighted mean, for f1(x1), and then use a factor model for f2(x2). This flexibility to allow non-parametric fits with relaxed assumptions on the actual relationship between response and predictor, provides the potential for better fits to data than purely parametric models, but arguably with some loss of interpretability. 

## Summary of advantages of GzLMs over traditional GLM regression

* We do not need to transform the response Y to have a normal distribution
* The choice of link is separate from the choice of random component thus we have more flexibility in modeling
* If the link produces additive effects, then we do not need constant variance.
* The models are fitted via Maximum Likelihood estimation; thus optimal properties of the estimators.
* All the inference tools and model checking that we will discuss for log-linear and logistic regression models apply for other GzLMs too; e.g., Wald and Likelihood ratio tests, Deviance, Residuals, Confidence intervals, Overdispersion.
* There is often one procedure in a software package to capture all the models listed above, e.g. PROC GENMOD in SAS or glm() in R, etc... with options to vary the three components.

But there are some limitations of GzLMs too, such as,

* Linear function, e.g. can have only a linear predictor in the systematic component
* Responses must be independent


## References

[Generalized linear model](https://en.wikipedia.org/wiki/Generalized_linear_model)

[Introduction to Generalized Linear Models](https://newonlinecourses.science.psu.edu/stat504/node/216/)

[Generalized Linear Models (GLZ)](http://www.statsoft.com/textbook/generalized-linear-models)

[Chapter 15 Generalized Linear Models](https://www.sagepub.com/sites/default/files/upm-binaries/21121_Chapter_15.pdf)

[Generalized additive model](https://en.wikipedia.org/wiki/Generalized_additive_model)


