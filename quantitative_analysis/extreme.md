# Extreme Value Theory

## Definition

Extreme value theory or extreme value analysis (EVA) is a branch of statistics dealing with the extreme deviations from the median of probability distributions. It seeks to assess, from a given ordered sample of a given random variable, the probability of events that are more extreme than any previously observed.

## Univariate theory

### Generalized extreme value distribution

| Notation           | $`GEV(\mu,\sigma,\gamma)`$ |
|--------------------|----------------------------|
| Location parameter | $`\mu`$                    |
| Scale parameter    | $`\sigma`$                 |
| Shape parameter    | $`\gamma`$                 |



## References

[Extreme value theory](https://en.wikipedia.org/wiki/Extreme_value_theory)

[Generalized extreme value distribution](https://en.wikipedia.org/wiki/Generalized_extreme_value_distribution)