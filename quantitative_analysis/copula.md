# Copula

In probability theory and statistics, a copula is a multivariate cumulative distribution function for which the marginal probability distribution of each variable is uniform. Copulas are used to describe the dependence between random variables. Copulas have been used widely in quantitative finance to model and minimize tail risk and portfolio-optimization applications.

## Dependence concepts

Copulas provide a natural way to study and measure dependence between random variables. Copula properties are invariant under strictly increasing transformations of the underlying random variables. Linear correlation (or Pearson’s correlation) is most frequently used in practice as a measure of dependence. However, since linear correlation is not a copula-based measure of dependence, it can often be quite misleading and should not be taken as the canonical dependence measure. Below we recall the basic properties of linear correlation, and then continue with some copula based measures of dependence.

### Perfect dependency

Perfect dependency when two risks can be written as increasing or decreasing functions of the same random variable. 

The couple $`X = (X_1,X_2)`$ is said comonotonic if there exists functions $`g_1`$ and $`g_2`$ not decreasing and a random variable Z such as :

$`X = (g_1(Z), g_2(Z))`$ by law. 

The couple $`X = (X_1,X_2)`$ is said anti-comonotonic if there exists a function $`g_1`$ not decreasing, a function $`g_2`$ not increasing and a random variable Z such as :

$`X = (g_1(Z), g_2(Z))`$ by law. 


### Concordance measure

A dependency measure $`\delta(.,.)`$ is a concordance measure if it possesses the following properties:

* P1: (symmetry) $`\delta(X_1,X_2) = \delta(X_2, X_1)`$
* P2: (normalisation) $`-1 \leq delta(X_1,X_2) \leq 1`$
* P3: $`\delta(X_1,X_2) = 1`$, if and only if $`X_1`$ and $`X_2`$ are comonotonic
* P4: $`\delta(X_1,X_2) = -1`$, if and only if $`X_1`$ and $`X_2`$ are anti-comonotonic
* P5: whether the monotonic function $`g:\mathbb{R} \rightarrow \mathbb{R}`$, $`\delta(g(X_1),X_2) = \delta(X_1,X_2)`$ if g is increasing or $`\delta(g(X_1),X_2) = -\delta(X_1,X_2)`$ if g is decreasing.

### Linear Correlation

Let (X,Y) be a vector of random variables with nonzero finite variances. The linear correlation coefficient for (X,Y) is $`\rho (X,Y) = \frac{Cov(X,Y)}{V(X) V(Y)}`$ where Cov(X,Y) = E(XY) − E(X)E(Y) is the covariance of (X,Y), and V(X) and V(Y) are the variances of X and Y.

Warning: the linear correlation is not a concordance measure ! Because it doesn't satisfy the P3 and P4 properties stated above. 

## Mathematical definition

Consider a random vector $`(X_{1},X_{2},\dots ,X_{d})`$. Suppose its marginals are continuous, i.e. the marginal CDFs $`F_{i} (x) = \Pr (X_{i} \leqslant x)`$ are continuous functions. By applying the probability integral transform to each component, the random vector

$`(U_{1},U_{2},\dots ,U_{d})=\left(F_{1}(X_{1}),F_{2}(X_{2}),\dots ,F_{d}(X_{d})\right)`$

has uniformly distributed marginals.

The copula of $`(X_{1},X_{2},\dots ,X_{d})`$ is defined as the joint cumulative distribution function of $`(U_{1},U_{2},\dots ,U_{d})`$:

$`C(u_{1},u_{2},\dots ,u_{d})=\mathrm {Pr} [U_{1}\leq u_{1},U_{2}\leq u_{2},\dots ,U_{d}\leq u_{d}]`$.

The copula C contains all information on the dependence structure between the components of $`(X_{1},X_{2},\dots ,X_{d})`$ whereas the marginal cumulative distribution functions $`F_{i}`$ contain all information on the marginal distributions.

The importance of the above is that the reverse of these steps can be used to generate pseudo-random samples from general classes of multivariate probability distributions. That is, given a procedure to generate a sample $`(U_{1},U_{2},\dots ,U_{d})`$ from the copula distribution, the required sample can be constructed as

$`(X_{1},X_{2},\dots ,X_{d})=\left(F_{1}^{-1}(U_{1}),F_{2}^{-1}(U_{2}),\dots ,F_{d}^{-1}(U_{d})\right)`$.

The inverses $`F_{i}^{-1}`$ are unproblematic as the $`F_{i}`$ were assumed to be continuous. The above formula for the copula function can be rewritten to correspond to this as:

$`C(u_{1},u_{2},\dots ,u_{d})=\mathrm {Pr} [X_{1}\leq F_{1}^{-1}(u_{1}),X_{2}\leq F_{2}^{-1}(u_{2}),\dots ,X_{d}\leq F_{d}^{-1}(u_{d})]`$.
	
## Probabilistic definition

In probabilistic terms, $`C:[0,1]^{d}\rightarrow [0,1]`$ is a d-dimensional copula if C is a joint cumulative distribution function of a d-dimensional random vector on the unit cube $`[0,1]^{d}`$ with uniform marginals.

In analytic terms, $`C:[0,1]^{d}\rightarrow [0,1]`$ is a d-dimensional copula if

* $`C(u_{1},\dots ,u_{i-1},0,u_{i+1},\dots ,u_{d})=0`$, the copula is zero if any one of the arguments is zero
* $`C(1,\dots ,1,u,1,\dots ,1)=u`$, the copula is equal to u if one argument is u and all others 1,
* C is d-non-decreasing, i.e., for each hyperrectangle $`B=\prod _{i=1}^{d}[x_{i},y_{i}]\subseteq [0,1]^{d}`$ the C-volume of B is non-negative: $`\int _{B}\mathrm {d} C(u)=\sum _{\mathbf {z} \in \times _{i=1}^{d}\{x_{i},y_{i}\}}(-1)^{N(\mathbf {z} )}C(\mathbf {z} )\geq 0`$, where the $`N(\mathbf {z} )=\#\{k:z_{k}=x_{k}\`$.

For instance, in the bivariate case, $`C:[0,1]\times [0,1]\rightarrow [0,1]`$ is a bivariate copula if $`C(0,u)=C(u,0)=0`$, $`C(1,u)=C(u,1)=u`$ and $`C(u_{2},v_{2})-C(u_{2},v_{1})-C(u_{1},v_{2})+C(u_{1},v_{1})\geq 0`$ for all $`0\leq u_{1}\leq u_{2}\leq 1`$ and $`0\leq v_{1}\leq v_{2}\leq 1`$.

## Sklar's theorem

Sklar's theorem provides the theoretical foundation for the application of copulas. It states that every multivariate cumulative distribution function

$`H(x_{1},\dots ,x_{d})=\mathrm {Pr} [X_{1}\leq x_{1},\dots ,X_{d}\leq x_{d}]`$

of a random vector $`(X_{1},X_{2},\dots ,X_{d})`$ can be expressed in terms of its marginals $`F_{i}(x_{i})=\mathrm {Pr} [X_{i}\leq x_{i}]`$ and a copula C. Indeed:

$`H(x_{1},\dots ,x_{d})=C\left(F_{1}(x_{1}),\dots ,F_{d}(x_{d})\right)`$.

In case that the multivariate distribution has a density h, and this is available, it holds further that

$`h(x_{1},\dots ,x_{d})=c(F_{1}(x_{1}),\dots ,F_{d}(x_{d}))\cdot f_{1}(x_{1})\cdot \dots \cdot f_{d}(x_{d})`$

where c is the density of the copula.

The theorem also states that, given H, the copula is unique on $`\operatorname {Ran} (F_{1})\times \cdots \times \operatorname {Ran} (F_{d})`$, which is the cartesian product of the ranges of the marginal cdf's. This implies that the copula is unique if the marginals $`F_{i}`$ are continuous.

The converse is also true: given a copula $`C:[0,1]^{d}\rightarrow [0,1]`$ and marginals $`F_{i}(x)`$ then $`C\left(F_{1}(x_{1}),\dots ,F_{d}(x_{d})\right)`$ defines a d-dimensional cumulative distribution function. 

## Stationary Condition

When time series are auto-correlated, they may generate a non existence dependence between sets of variables and result in incorrect Copula dependence structure. 

## Fréchet–Hoeffding copula bounds

The Fréchet–Hoeffding Theorem states that for any Copula $`C:[0,1]^{d}\rightarrow [0,1]`$ and any $`(u_{1},\dots ,u_{d})\in [0,1]^{d}`$ the following bounds hold:

$`W(u_{1},\dots ,u_{d})\leq C(u_{1},\dots ,u_{d})\leq M(u_{1},\dots ,u_{d})`$

The function W is called lower Fréchet–Hoeffding bound and is defined as

$`W(u_{1},\ldots ,u_{d})=\max \left\{1-d+\sum \limits _{i=1}^{d}{u_{i}},\,0\right\}`$

The function M is called upper Fréchet–Hoeffding bound and is defined as

$`M(u_{1},\ldots ,u_{d})=\min\{u_{1},\dots ,u_{d}\}`$.

The upper bound is sharp: M is always a copula, it corresponds to comonotone random variables.

The lower bound is point-wise sharp, in the sense that for fixed u, there is a copula $`{\tilde {C}}`$ such that $`{\tilde {C}}(u)=W(u)`$. However, W is a copula only in two dimensions, in which case it corresponds to countermonotonic random variables.

In two dimensions, i.e. the bivariate case, the Fréchet–Hoeffding Theorem states

$`\max\{u+v-1,\,0\}\leq C(u,v)\leq \min\{u,v\}`$.
	
## Families of copulas

### Gaussian copula

### Archimedean copulas

## Expectation for copula models and Monte Carlo integration

In statistical applications, many problems can be formulated in the following way. One is interested in the expectation of a response function $`g:\mathbb {R} ^{d}\rightarrow \mathbb {R}`$ applied to some random vector $`(X_{1},\dots ,X_{d})`$. If we denote the cdf of this random vector with H, the quantity of interest can thus be written as

$`\mathrm {E} \left[g(X_{1},\dots ,X_{d})\right]=\int _{\mathbb {R} ^{d}}g(x_{1},\dots ,x_{d})\,\mathrm {d} H(x_{1},\dots ,x_{d})}`$.

If H is given by a copula model, i.e.,

$`H(x_{1},\dots ,x_{d})=C(F_{1}(x_{1}),\dots ,F_{d}(x_{d}))`$

this expectation can be rewritten as

$`\mathrm {E} \left[g(X_{1},\dots ,X_{d})\right]=\int _{[0,1]^{d}}g(F_{1}^{-1}(u_{1}),\dots ,F_{d}^{-1}(u_{d}))\,\mathrm {d} C(u_{1},\dots ,u_{d})`$.

In case the copula C is absolutely continuous, i.e. C has a density c, this equation can be written as

$`\mathrm {E} \left[g(X_{1},\dots ,X_{d})\right]=\int _{[0,1]^{d}}g(F_{1}^{-1}(u_{1}),\dots ,F_{d}^{-1}(u_{d}))\cdot c(u_{1},\dots ,u_{d})\,du_{1}\cdots \mathrm {d} u_{d},}`$

and if each marginal distribution has the density f i {\displaystyle f_{i}} f_{i} it holds further that

$`\mathrm {E} \left[g(X_{1},\dots ,X_{d})\right]=\int _{\mathbb {R} ^{d}}g(x_{1},\dots x_{d})\cdot c(F_{1}(x_{1}),\dots ,F_{d}(x_{d}))\cdot f_{1}(x_{1})\cdots f_{d}(x_{d})\,\mathrm {d} x_{1}\cdots \mathrm {d} x_{d}.`$

If copula and margins are known (or if they have been estimated), this expectation can be approximated through the following Monte Carlo algorithm:

    Draw a sample ( U 1 k , … , U d k ) ∼ C ( k = 1 , … , n ) {\displaystyle (U_{1}^{k},\dots ,U_{d}^{k})\sim C\;\;(k=1,\dots ,n)} (U_{1}^{k},\dots ,U_{d}^{k})\sim C\;\;(k=1,\dots ,n) of size n from the copula C
    By applying the inverse marginal cdf's, produce a sample of ( X 1 , … , X d ) {\displaystyle (X_{1},\dots ,X_{d})} (X_{1},\dots ,X_{d}) by setting ( X 1 k , … , X d k ) = ( F 1 − 1 ( U 1 k ) , … , F d − 1 ( U d k ) ) ∼ H ( k = 1 , … , n ) {\displaystyle (X_{1}^{k},\dots ,X_{d}^{k})=(F_{1}^{-1}(U_{1}^{k}),\dots ,F_{d}^{-1}(U_{d}^{k}))\sim H\;\;(k=1,\dots ,n)} (X_{1}^{k},\dots ,X_{d}^{k})=(F_{1}^{-1}(U_{1}^{k}),\dots ,F_{d}^{-1}(U_{d}^{k}))\sim H\;\;(k=1,\dots ,n)
    Approximate E [ g ( X 1 , … , X d ) ] {\displaystyle \mathrm {E} \left[g(X_{1},\dots ,X_{d})\right]} {\displaystyle \mathrm {E} \left[g(X_{1},\dots ,X_{d})\right]} by its empirical value:

            E [ g ( X 1 , … , X d ) ] ≈ 1 n ∑ k = 1 n g ( X 1 k , … , X d k ) {\displaystyle \mathrm {E} \left[g(X_{1},\dots ,X_{d})\right]\approx {\frac {1}{n}}\sum _{k=1}^{n}g(X_{1}^{k},\dots ,X_{d}^{k})} {\displaystyle \mathrm {E} \left[g(X_{1},\dots ,X_{d})\right]\approx {\frac {1}{n}}\sum _{k=1}^{n}g(X_{1}^{k},\dots ,X_{d}^{k})}


## Empirical copulas

When studying multivariate data, one might want to investigate the underlying copula. Suppose we have observations

$`(X_{1}^{i},X_{2}^{i},\dots ,X_{d}^{i}),\,i=1,\dots ,n`$

from a random vector $`(X_{1},X_{2},\dots ,X_{d})`$ with continuous margins. The corresponding "true" copula observations would be

$`(U_{1}^{i},U_{2}^{i},\dots ,U_{d}^{i})=\left(F_{1}(X_{1}^{i}),F_{2}(X_{2}^{i}),\dots ,F_{d}(X_{d}^{i})\right),\,i=1,\dots ,n.`$

However, the marginal distribution functions F i {\displaystyle F_{i}} F_{i} are usually not known. Therefore, one can construct pseudo copula observations by using the empirical distribution functions

$`F_{k}^{n}(x)={\frac {1}{n}}\sum _{i=1}^{n}\mathbf {1} (X_{k}^{i}\leq x)`$

instead. Then, the pseudo copula observations are defined as

$`({\tilde {U}}_{1}^{i},{\tilde {U}}_{2}^{i},\dots ,{\tilde {U}}_{d}^{i})=\left(F_{1}^{n}(X_{1}^{i}),F_{2}^{n}(X_{2}^{i}),\dots ,F_{d}^{n}(X_{d}^{i})\right),\,i=1,\dots ,n.`$

The corresponding empirical copula is then defined as

$`C^{n}(u_{1},\dots ,u_{d})={\frac {1}{n}}\sum _{i=1}^{n}\mathbf {1} \left({\tilde {U}}_{1}^{i}\leq u_{1},\dots ,{\tilde {U}}_{d}^{i}\leq u_{d}\right)`$

The components of the pseudo copula samples can also be written as $`{\tilde {U}}_{k}^{i}=R_{k}^{i}/n`$, where $`R_{k}^{i}`$ is the rank of the observation $`X_{k}^{i}`$:

$`R_{k}^{i}=\sum _{j=1}^{n}\mathbf {1} (X_{k}^{j}\leq X_{k}^{i})`$

Therefore, the empirical copula can be seen as the empirical distribution of the rank transformed data. 

## Applications in quantitative finance

n quantitative finance copulas are applied to risk management, to portfolio management and optimization, and to derivatives pricing.

For the former, copulas are used to perform stress-tests and robustness checks that are especially important during "downside/crisis/panic regimes" where extreme downside events may occur (e.g., the global financial crisis of 2007–2008). The formula was also adapted for financial markets and was used to estimate the probability distribution of losses on pools of loans or bonds.

During a downside regime, a large number of investors who have held positions in riskier assets such as equities or real estate may seek refuge in 'safer' investments such as cash or bonds. This is also known as a flight-to-quality effect and investors tend to exit their positions in riskier assets in large numbers in a short period of time. As a result, during downside regimes, correlations across equities are greater on the downside as opposed to the upside and this may have disastrous effects on the economy.[19][20] For example, anecdotally, we often read financial news headlines reporting the loss of hundreds of millions of dollars on the stock exchange in a single day; however, we rarely read reports of positive stock market gains of the same magnitude and in the same short time frame.

Copulas aid in analyzing the effects of downside regimes by allowing the modelling of the marginals and dependence structure of a multivariate probability model separately. For example, consider the stock exchange as a market consisting of a large number of traders each operating with his/her own strategies to maximize profits. The individualistic behaviour of each trader can be described by modelling the marginals. However, as all traders operate on the same exchange, each trader's actions have an interaction effect with other traders'. This interaction effect can be described by modelling the dependence structure. Therefore, copulas allow us to analyse the interaction effects which are of particular interest during downside regimes as investors tend to herd their trading behaviour and decisions. (See also agent-based computational economics, where price is treated as an emergent phenomenon, resulting from the interaction of the various market participants, or agents.)

The users of the formula have been criticized for creating "evaluation cultures" that continued to use simple copulæ despite the simple versions being acknowledged as inadequate for that purpose.[21] Thus, previously, scalable copula models for large dimensions only allowed the modelling of elliptical dependence structures (i.e., Gaussian and Student-t copulas) that do not allow for correlation asymmetries where correlations differ on the upside or downside regimes. However, the recent development of vine copulas[22] (also known as pair copulas) enables the flexible modelling of the dependence structure for portfolios of large dimensions.[23] The Clayton canonical vine copula allows for the occurrence of extreme downside events and has been successfully applied in portfolio optimization and risk management applications. The model is able to reduce the effects of extreme downside correlations and produces improved statistical and economic performance compared to scalable elliptical dependence copulas such as the Gaussian and Student-t copula.[24]

Other models developed for risk management applications are panic copulas that are glued with market estimates of the marginal distributions to analyze the effects of panic regimes on the portfolio profit and loss distribution. Panic copulas are created by Monte Carlo simulation, mixed with a re-weighting of the probability of each scenario.[25]

As regards derivatives pricing, dependence modelling with copula functions is widely used in applications of financial risk assessment and actuarial analysis – for example in the pricing of collateralized debt obligations (CDOs).[26] Some believe the methodology of applying the Gaussian copula to credit derivatives to be one of the reasons behind the global financial crisis of 2008–2009;[27][28][29] see David X. Li § CDOs and Gaussian copula.

Despite this perception, there are documented attempts within the financial industry, occurring before the crisis, to address the limitations of the Gaussian copula and of copula functions more generally, specifically the lack of dependence dynamics. The Gaussian copula is lacking as it only allows for an elliptical dependence structure, as dependence is only modeled using the variance-covariance matrix.[24] This methodology is limited such that it does not allow for dependence to evolve as the financial markets exhibit asymmetric dependence, whereby correlations across assets significantly increase during downturns compared to upturns. Therefore, modeling approaches using the Gaussian copula exhibit a poor representation of extreme events.[24][30] There have been attempts to propose models rectifying some of the copula limitations.[30][31][32]

Additional to CDOs, Copulas have been applied to other asset classes as a flexible tool in analyzing multi-asset derivative products. The first such application outside credit was to use a copula to construct a basket implied volatility surface,[33] taking into account the volatility smile of basket components. Copulas have since gained popularity in pricing and risk management[34] of options on multi-assets in the presence of a volatility smile, in equity-, foreign exchange- and fixed income derivatives. 

## References

[Copula (probability theory)](https://en.wikipedia.org/wiki/Copula_(probability_theory))

[Modelling Dependence with Copulas and Applications to Risk Management](https://people.math.ethz.ch/~embrecht/ftp/copchapter.pdf)
