# Value at Risk (VaR)

## Definition

Value at risk (VaR) is a measure of the risk of loss for investments. It estimates how much a set of investments might lose (with a given probability), given normal market conditions, in a set time period such as a day. VaR is typically used by firms and regulators in the financial industry to gauge the amount of assets needed to cover possible losses.

For a given portfolio, time horizon, and probability p, the p VaR can be defined informally as the maximum possible loss during that time after we exclude all worse outcomes whose combined probability is at most p. This assumes mark-to-market pricing, and no trading in the portfolio.

Measuring potential losses: VaR (value at risk) is defined as the maximum loss a portfolio will experience at a specified confidence level over a specified time horizon. Sometimes VaR is written $`VaR_\alpha`$ and the confidence level is indicated as a superscript, $`\alpha`$.

## Ideas behin VaR

A VaR statistic has three components: 

* a time period
* a confidence level
* a loss amount (or loss percentage). 

Keep these three parts in mind as we give some examples of variations of the question that VAR answers:

* What is the most I can - with a 95% or 99% level of confidence - expect to lose in dollars over the next month?
* What is the maximum percentage I can - with 95% or 99% confidence - expect to lose over the next year?

You can see how the "VaR question" has three elements: a relatively high level of confidence (typically either 95% or 99%), a time period (a day, a month or a year) and an estimate of investment loss (expressed either in dollar or percentage terms).


## Mathematical definition

TBD from Wikipedia 

## References

[Value at Risk](https://en.wikipedia.org/wiki/Value_at_risk)

[An Introduction to Value at Risk (VAR)](https://www.investopedia.com/articles/04/092904.asp)
