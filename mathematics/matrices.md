# Matrices

## Matrix Multiplication

If A is an n × m matrix and B is an m × p matrix, the matrix product C = AB (denoted without multiplication signs or dots) is defined to be the n × p matrix where $`c_{ij} = \sum_{k=1}^{m} a_{ik} b_{kj}`$ (for i = 1, ..., n and j = 1, ..., p). 

> The best way to think about matrix multiplication is as a *transformation of your data*

## The Hadamard Product

Element-wise product of two matrices A and B (sizes i x j) is given by:

$`A \odot B = \begin{pmatrix}a_{ij} b_{ij}\end{pmatrix}`$

## Product Properties

Distributivity: A (B + C) = AB + AC

Associativity: A (BC) = (AB) C

Matrix multiplication is commutative if - and only when - both of the matrices are diagonal and of equal dimensions. 

Identity matrix: I A = A, with I being filled with ones on the diagonal and zeroes elsewhere. 

Hadamard product is Distributive, Associative, and Commutative.

## Geometry Matrix Operation

The determinant of a square matrix is the factor the area is multiplied by. The determinant can be seen as the scaling factor.

## Determinant Computation

For a (2,2) matrix:

$`|D| = \begin{vmatrix} a & b  \\ c & d  \\ \end{vmatrix} = a d - b c`$

For a (3,3) matrix:

$`|A| = \begin{vmatrix} a & b & c \\ d & e & f \\ g & h & i \end{vmatrix}`$

$`|A| = a \begin{vmatrix} \Box & \Box & \Box \\ \Box & e & f \\ \Box & h & i \end{vmatrix} - b \begin{vmatrix} \Box & \Box & \Box \\ d & \Box & f \\ g & \Box & i \end{vmatrix} + c \begin{vmatrix} \Box & \Box & \Box \\ d & e & \Box \\ g & h & \Box \end{vmatrix}`$

$`|A| = aei + bfg + cdh - ceg - bdi - afh`$

Property:

$`det(A) = det(A^T)`$

## Invertibility

### When you can invert

When det(A) is different of 0.

$`A^{-1} A = I`$

### How to compute the inverse

For a (2,2) matrix $`A = \begin{pmatrix} a & b  \\ c & d  \\ \end{pmatrix}`$:

$`A^{-1} = \frac{1}{det(A)} \begin{pmatrix} d & -b  \\ -c & a  \\ \end{pmatrix}`$

## Linear Dependence

Linear dependency of vectors - whether the given vectors all lie in single plane or not - and see how linear dependency defines whether the inverse of a matrix exists.

$`\vec{v_1}, \vec{v_2}, ..., \vec{v_k}`$ are linearly dependant if $`\exists a_1, a_2, ..., a_k`$ NOT ALL ZERO such as:

$`a_1 \vec{v_1} + a_2 \vec{v_2} + ... + a_k \vec{v_k} = \vec{0}`$

