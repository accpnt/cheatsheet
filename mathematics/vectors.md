# Vectors

## Definition

A vector describes a movement from one point to another. A vector quantity has both direction and magnitude (size). A scalar quantity has only magnitude.

## Properties

![vecadd](./assets/vector_a_plus_b.png)

## Norms

| Norm         | Formula                                                                |
|--------------|------------------------------------------------------------------------|
| $`L_p`$      | $`\|\vec{x}\|_p = (\sum_{i=1}^{n} {\mid x_i \mid}^{p})^{\frac{1}{p}}`$ |
| $`L_2`$      | $`\|\vec{x}\|_2 = \sqrt{\sum_{i=1}^{n} {x_i}^{2}}`$                    |
| $`L_1`$      | $`\|\vec{x}\|_1 = \sum_{i=1}^{n} \mid x_i \mid`$                       |
| $`L_\infty`$ | $`\|\vec{x}\|_\infty = sup_{n}(\mid x_i \mid)`$                        |

* $`L_0`$ is not a norm, it corresponds to the total number of non-zero elements in a vector.
* $`L_1`$ is the sum of the magnitudes of the vectors in a space. It is the most natural way of measure distance between vectors, that is the sum of absolute difference of the components of the vectors. In this norm, all the components of the vector are weighted equally.
* $`L_2`$ is the most popular norm, also known as the Euclidean norm. It is the shortest distance to go from one point to another.
* $`L_\infty`$ gives the largest magnitude among each element of a vector (only the largest element has any effect).
^

## Dot Products

### Definition

Given two vectors $`\vec{u}`$ and $`\vec{v}`$ of sizes n, the dot product is defined as $`\vec{u} \cdot \vec{v} = \sum_{i=1}^{n} u_i v_i`$. 

We also have $`\vec{u} \cdot \vec{v} = \|\vec{u}\| \|\vec{v}\| \cos(\theta)`$, where $`\theta`$ is the angle between u and v. 

### Orthogonality

Two vectors are orthogonal if $`\vec{u} \cdot \vec{v} = 0`$, so if $`\theta = \frac{\pi}{2}`$ or $`\theta = - \frac{\pi}{2}`$ (if vectors u or v aren't null vectors). 

## References

[L0 Norm, L1 Norm, L2 Norm & L-Infinity Norm](https://medium.com/@montjoile/l0-norm-l1-norm-l2-norm-l-infinity-norm-7a7d18a4f40c)