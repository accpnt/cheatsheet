# Derivatives and Integrals

## Overview

![derint](./assets/derint.gif)

## Derivatives

### Usual derivatives

| f(x)                   | I                                                             | f'(x)                    |
|------------------------|---------------------------------------------------------------|--------------------------|
| $`\lambda`$ (constant) | $`\R`$                                                        | 0                        |
| x                      | $`\R`$                                                        | 1                        |
| $`x^n`$                | $`\R`$                                                        | $`n x^{n-1}`$            |
| $`\frac{1}{x}`$        | $`]-\infty, 0[`$ or $`]0, +\infty[`$                          | $`-\frac{1}{x^2}`$       |
| $`\frac{1}{x^n}`$      | $`]-\infty, 0[`$ or $`]0, +\infty[`$                          | $`\frac{n}{x^{n+1}}`$    |
| $`\sqrt(x)`$           | $`]0, +\infty [`$                                             | $`\frac{1}{2 \sqrt(x)}`$ |
| $`\ln(x)`$             | $`]0, +\infty [`$                                             | $`\frac{1}{x}`$          |
| $`e^x`$                | $`\R`$                                                        | $`e^x`$                  |
| $`\sin(x)`$            | $`\R`$                                                        | $`\cos(x)`$              |
| $`\cos(x)`$            | $`\R`$                                                        | $`-\sin(x)`$             |
| $`\tan(x)`$            | $`]-\frac{\pi}{2} + k \pi, \frac{\pi}{2} + k \pi[, k \in \Z`$ | $`1 + tan^2(x)`$         |

### Properties

$`(f + g)' = f' + g'`$

$`(\lambda f)' = \lambda f'`$

$`(fg)' = f'g + fg'`$

$`(\frac{1}{f})' = -\frac{f'}{f^2}`$

$`(\frac{f}{g})' = -\frac{f'g - fg'}{g^2}`$

$`(f \circ g)' = g' (f' \circ g)`$

## Integrals

### Usual integrals

| f(x)                   | I                                                             | F(x)                              |
|------------------------|---------------------------------------------------------------|-----------------------------------|
| $`\lambda`$ (constant) | $`\R`$                                                        | $`\lambda x + C`$                 |
| $`x`$                  | $`\R`$                                                        | $`\frac{x^2}{2} + C`$             |
| $`x^n, n \in \N^*`$    | $`\R`$                                                        | $`\frac{x^{n + 1}}{n + 1} + C`$   |
| $`\frac{1}{x}`$        | $`]-\infty, 0[`$ or $`]0, +\infty[`$                          | $`\ln \mid x \mid + C`$           |
| $`\frac{1}{x^n}`$      | $`]-\infty, 0[`$ or $`]0, +\infty[`$                          | $`- \frac{1}{(n-1) x^{n-1}} + C`$ |
| $`\frac{1}{\sqrt(x)}`$ | $`]0, +\infty[`$                                              | $`2 \sqrt(x) + C`$                |
| $`\ln(x)`$             | $`{\R_+}^*`$                                                  | $`x \ln(x) - x + C`$              |
| $`e^x`$                | $`\R`$                                                        | $`e^x + C`$                       |
| $`\sin(x)`$            | $`\R`$                                                        | $`-\cos(x) + C`$                  |
| $`\cos(x)`$            | $`\R`$                                                        | $`\sin(x) + C`$                   |
| $`1 + tan^2(x)`$       | $`]-\frac{\pi}{2} + k \pi, \frac{\pi}{2} + k \pi[, k \in \Z`$ | $`\tan(x) + C`$                   |

### Properties

We suppose that the function u is differentiable on an interval I. 

A primitive of $`u' u^n`$ on I is $`\frac{u^{n+1}}{n+1}, n \in \N^*`$

A primitive of $`\frac{u'}{u^2}`$ on I is $`- \frac{1}{u}`$

A primitive of $`\frac{u'}{u^n}`$ on I is $`- \frac{1}{(n-1) u^{n-1}}, n \in \N, n \geq 2`$

A primitive of $`\frac{u'}{\sqrt(u)}`$ on I is $`2 \sqrt(u)`$, if we suppose u > 0 on I 

A primitive of $`\frac{u'}{u}`$ on I is $`\ln \mid u \mid`$

A primitive of $`u' e^u`$ on I is $`e^u`$

## References

[Tableau de dérivées et intégrales usuelles](https://www.math.u-bordeaux.fr/~glazzari/tableaux.pdf)

[Tableaux des dérivées](https://www.math.u-bordeaux.fr/~cdubuiss/Formulaire/2015%20-%20tableaux%20d%C3%A9riv%C3%A9es,%20primitives,%20DL.pdf)

[Derivatives and Integrals](http://hyperphysics.phy-astr.gsu.edu/hbase/Math/derint.html)