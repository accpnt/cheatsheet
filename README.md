# Cheatsheet

## Finance

* [Value at Risk](./finance/value_at_risk.md)

## Machine Learning

To be done

## Mathematics

* [Derivatives and Integrals](./mathematics/derivatives_integrals.md)
* [Matrices](./mathematics/matrices.md)
* [Vectors](./mathematics/vectors.md)

## Probabilities

* [Distributions](./probabilities/distributions.md)
* [Probabilities](./probabilities/probabilities.md)
* [Random variables](./probabilities/random_variables.md)

## Quantitative Analysis

* [Copula](./quantitative_analysis/copula.md)
* [Extreme Value Theory](./quantitative_analysis/extreme.md)

## Statistics

* [ANOVA](./statistics/anova.md)
* [Categorical variables](./statistics/categorical.md)
* [Cheatsheet](./statistics/cheatsheet.md)
* [Degrees of freedom](./statistics/degrees_of_freedom.md)
* [General Linear Models](./statistics/general_linear_model.md)
* [Generalized Linear Models](./statistics/generalized_linear_model.md)
* [Geostatistics](./statistics/geostatistics.md)
* [Glossary](./statistics/glossary.md)
* [Logistic Regression](./statistics/logistic_regression.md)
* [Mixed Models](./statistics/mixed_models.md)
* [p-value](./statistics/pvalue.md)
* [Repeated Measures](./statistics/repeated_measures.md)
* [Surveys](./statistics/surveys.md)
* [Survival Analysis](./statistics/survival_analysis.md)
* [Tests](./statistics/tests.md)
* [Time Series (ARIMA)](./statistics/time_series_arima.md)
* [Time Series (ARCH/GARCH)](./statistics/time_series_garch.md)
