# Probabilities

## Axioms of probability

* Sample space: The set of all possible outcomes of an experiment is known as the sample space of the experiment and is denoted by S
* Event: Any subset E of the sample space is known as an event. That is, an event is a set consisting of possible outcomes of the experiment. If the outcome of the experiment is contained in E, then we say that E has occurred.

For each event E, we denote $`\Pr(E)`$ as the probability of event E occuring.

### Axiom 1

Every probability is between 0 and 1 included.

$`0 \leqslant \Pr(E) \leqslant 1`$

$`\Pr(\overline{E}) = 1 - \Pr(E)`$

### Axiom 2

The probability that at least one of the elementary events in the entire sample space will occur is 1.

$`\Pr(S) = 1`$

### Axiom 3

For any sequence of mutually exclusive events $`E_1, E_2, E_3, ... E_n`$, we have:

$`\Pr(\bigcup_{i=1}^{n} E_i)= \sum_{i=1}^{n} \Pr(E_i)`$

## Probability rules

### De Morgan's laws

$`\overline{A \cup B} = \overline{A} \cap \overline{B}`$

$`\overline{A \cap B} = \overline{A} \cup \overline{B}`$

### Addition rule

$`\Pr(A \cup B) = \Pr(A) + \Pr(B) - \Pr(A \cap B)`$

if A and B are mutually exclusive: $`\Pr(A \cup B) = \Pr(A) + \Pr(B)`$

For A, B, and C events: 

$`\Pr(A \cup B \cup C) = \Pr(A) + \Pr(B) + \Pr(C) − \Pr(A \cap B) − \Pr(A \cap C) − \Pr(B \cap C) + \Pr(A \cap B \cap C)`$

### Multiplication rule

$`\Pr(A \cap B) = \Pr(A)  \Pr(B | A) = \Pr(B)  \Pr(A | B)`$

if A and B are independant: $`\Pr(A \cap B) = \Pr(A) \Pr(B)`$

## Conditional Probability

### Bayes' rule

$`\Pr(A | B) = \frac{\Pr(B | A) \Pr(A)}{\Pr(B)}`$

$`\Pr(A | B) = \frac{\Pr(A \cap B)}{\Pr(B)}`$

Which leads to:

$`\Pr(A | B) = \frac{\Pr(B | A) \Pr(A)}{\Pr(B | A) \Pr(A) + \Pr(B | \overline{A}) \Pr(\overline{A})}`$

### Independance

$`\Pr(A | B) = \Pr(A)`$

$`\Pr(B | A) = \Pr(B)`$

### Law of Total Probability 

Let $`B_1, B_2, B_3, ... B_n`$ be a partition of the sample space (i.e., they are disjoint and their union is the entire sample space).

$`\Pr(A) = \Pr(A | B_1) \Pr(B_1) + \Pr(A | B_2) \Pr(B_2) + ··· + \Pr(A | B_n) \Pr(B_n)`$

$`\Pr(A) = \Pr(A \cap B_1) + \Pr(A \cap B_2) + ··· + \Pr(A \cap B_n)`$

For A and B:

$`\Pr(B) = \Pr(A \cap B) + \Pr(B \cap \overline{A})`$

## References

[Probability Cheatsheet v1.1.1](https://www.andrew.cmu.edu/user/lakoglu/courses/95801/docs/probability_cheatsheet.pdf)

[Probability For Dummies Cheat Sheet](https://www.dummies.com/education/math/probability/probability-for-dummies-cheat-sheet/)

[Probability cheatsheet](https://stanford.edu/~shervine/teaching/cme-106/cheatsheet-probability)

[http://www.wzchen.com/probability-cheatsheet](http://www.wzchen.com/probability-cheatsheet)
