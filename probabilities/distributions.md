# Probability distributions

## Discrete case

### Binomial

![binomial](./assets/dist-binomial.png)

$`X \sim B(n,p)`$

$`\Pr(X = x) = \binom{n}{x} p^x q^{n-x}`$

$`E(X) = np`$

$`V(X) = npq`$

### Poisson

![poisson](./assets/dist-poisson.png)

$`X \sim Po(\mu)`$

$`\Pr(X = x) = \frac{\mu^x}{x!} e^{-\mu}`$

$`E(X) = \mu`$

$`V(X) = \mu`$

## Continuous case

### Uniform

![uniform](./assets/dist-uniform.png)

$`X \sim U(a,b)`$

$`\Pr(X = x) = \frac{1}{b - a}`$

$`E(X) = \frac{a + b}{2}`$

$`V(X) = \frac{(b - a)^2}{12}`$

## Gaussian

![normal](./assets/dist-normal.png)

$`X \sim N(\mu,\sigma)`$

$`\Pr(X = x) = \frac{1}{\sqrt{2 \pi} \sigma} e^{- \frac{1}{2} (\frac{x - \mu}{\sigma})^2}`$

$`E(X) = \mu`$

$`V(X) = \sigma^2`$

## Exponential

![exponential](./assets/dist-exponential.png)

$`X \sim E(\lambda)`$

$`\Pr(X = x) = \lambda e^{- \lambda x}`$

$`E(X) = \frac{1}{\lambda}`$

$`V(X) = \frac{1}{\lambda^2}`$

## Chebyshev's inequality

Let X be a random variable with expected value $`\mu`$. For k, $`\sigma`$ > 0, we have the following inequality:

$`\Pr(|X - \mu | \geq k \sigma) \leq \frac{1}{k^2}`$

![Chebyshev](./assets/chebyshev.png)

## References

[Probability distributions](https://stanford.edu/~shervine/teaching/cme-106/cheatsheet-probability#probability-distributions)

[Probability Cheatsheet v1.1.1](https://www.andrew.cmu.edu/user/lakoglu/courses/95801/docs/probability_cheatsheet.pdf)
