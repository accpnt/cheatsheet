# Random variables

## Definitions

### Random variable

A random variable, often noted X, is a function that maps every element in a sample space to a real line.

### Expected value and variance

$`E(X) = \sum_{i=1}{n} x_i \Pr(X = x_i)`$ 

Where $`x_i`$ are the finite outcomes.

Expected value has the following properties:

$`E(a X + b) = a E(X) + b`$

$`E(X + Y) = E(X) + E(Y)`$

$`Var(X) = \sum_{i=1}{n} (x_i - E(X))^2 = E(X^2) - {E(X)}^2`$

We have the following properties:

$`Var(a X + b) = a^2 Var(X)`$

$`Var(X + Y) = Var(X) + Var(Y) + 2 Cov(X,Y)`$

With $`Cov(X,Y) = E(XY) - E(X) E(Y)`$

If X and Y are independant, we have: $`E(XY) = E(X) E(Y)`$, so $`Cov(X,Y) = 0`$

### Cumulative distribution function (CDF)

The cumulative distribution function F, which is monotonically non-decreasing and is such that $`\lim_{x \to -\infty} F(x) = 0`$ and $`\lim_{x \to +\infty} F(x) = 0`$, is defined as: $`F(x) = \Pr(X \leqslant x)`$. 

![cdf](./assets/cdf.png)

Remark: we have $`\Pr(a < X \leqslant B) = F(b) − F(a)`$

### Probability density function (PDF) 

The probability density function f is the probability that X takes on values between two adjacent realizations of the random variable.

## Relationships involving the PDF and CDF

### Discrete case

Here, X takes discrete values, such as outcomes of coin flips. By noting f and F the PDF and CDF respectively, we have the following relations:

$`F(x) = \sum_{xi \leqslant x} \Pr(X = x_i)`$ and $`f(x_j) = \Pr(X = xj)`$

On top of that, the PDF is such that:

$`0 \leqslant f(x_j) \leqslant 1`$ and $`\sum_{j} f(x_j) = 1`$

### Continuous case

Here, X takes continuous values, such as the temperature in the room. By noting f and F the PDF and CDF respectively, we have the following relations:

$`F(x) = \int_{-\infty}^{x} f(y) dy`$ and $`f(x) = \frac{dF}{dx}`$

On top of that, the PDF is such that:

$`0 \leqslant f(x)`$ and $`\int_{-\infty}^{+\infty}f(x) dx = 1`$


