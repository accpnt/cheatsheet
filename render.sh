#!/bin/sh

# This script uses pandoc to export all markdown files to PDF. All markdown files are copied to an output folder. The trick is that GitLab uses backticks to fully render the LaTeX equations. So we need to remove them all before rendering. 


OUTPUT_DIR="output"

mkdir -p $OUTPUT_DIR

for d in */ ; do
	echo "$d"
	cp -r $d/* $OUTPUT_DIR
done

cd $OUTPUT_DIR

for file in *.md; do
	[ -f "$file" ] || break
	sed -i -e "s/\`//g" $file
	pandoc $file --pdf-engine=xelatex -V geometry:margin=1in -o $file.pdf
	rm $file
done
